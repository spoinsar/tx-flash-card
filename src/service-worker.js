// This optional code is used to register a service worker.
// register() is not called by default.

// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.

// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA

let APP_VERSION="1.0";
let ROOT_CACHE="offlinewebsite-"+APP_VERSION;
let CONTENT_CACHE="offlinewebsite-content";

// without trailing slash !
let CONTENT_URL=[".quiz", ".png", ".jpeg", ".jpg"];
let FILELIST="filelist.txt"
let IMPORT_URL = "http://localhost:3000/questions"

let filesToCache = [
];

/* eslint-disable-next-line no-restricted-globals */
self.addEventListener("install", function(e) {
  e.waitUntil(caches.open(ROOT_CACHE).then((cache) =>{
    return cache.addAll(filesToCache);
  }));
    console.log("service worker installed...");
});


// only return a response when the file exists in the cache
const getResponseFromCache = (cacheName, request) => {
  return caches.open(cacheName)
    .then(cache => {
      return cache.match(request);
    });
};

// store a request / response pair in the cache
const setResponseCache = (cacheName, request, response) => {
  return caches.open(cacheName)
    .then(cache => {
      return cache.put(request, response);
    });
};

// cache first policy : return from cache if possible, from network if not (and make sure to fill the cache)
const getResponseFromCacheFirst = (cacheName, request) => {
  // retrieve in cache
  const response = getResponseFromCache(cacheName, request)
    .then((response) => {
      if (response) {
        // return response from cache if found
        console.log("cache hit: "+request.url);
        return response;
      } else {
        // request if not found
        console.log("cache miss: "+request.url);
        return fetch(request)
          .then(response => {
            if (response.ok) {
              // clone in cache when request succeded
              setResponseCache(cacheName, request, response.clone());
            } else {
              console.log("cache miss + fetch error: "+request.url);
              return null;
            }

            // return response for usage
            return response;
          });
      }
    });

  return response;
};

// cache last policy : return from network if possible, from cache if not (and make sure to fill the cache)
const getResponseFromCacheLast = (cacheName, request) => {
  // retrieve in cache
  const response = fetch(request)
    .then(response => {
      if (response.ok) {
        // clone in cache when request succeded
        setResponseCache(cacheName, request, response.clone());
        console.log("fetch success: "+request.url);
        return response;
      } else {
        console.log("fetch error: "+request.url);
        return getResponseFromCache(cacheName, request)
          .then((response) => {
          if (response) {
            // return response from cache if found
            console.log("cache hit: "+request.url);
          } else {
            // request if not found
            console.log("cache miss + fetch error: "+request.url);
            return null;
          }
          return response;
        });
      }
    })
    .catch(err => {
      console.log("fetch error: "+request.url);
      console.log("Error: "+err);
      return getResponseFromCache(cacheName, request)
        .then((response) => {
        if (response) {
          // return response from cache if found
          console.log("cache hit: "+request.url);
        } else {
          // request if not found
          console.log("cache miss + fetch error: "+request.url);
          return null;
        }
        return response;
      });
    });
  return response;
};

/* eslint-disable-next-line no-restricted-globals */
self.addEventListener("fetch", event => {
  const requestUrl = new URL(event.request.url);

  // dont mix up data with app code
  let cacheName;
  if (CONTENT_URL.some(v => requestUrl.pathname.includes(v))) {
    cacheName=CONTENT_CACHE;
    const isCacheFirst = requestUrl.searchParams.get('isCacheFirst');
    if (isCacheFirst === "true") {
      event.respondWith(getResponseFromCacheFirst(cacheName, event.request));
    }
    else{
      event.respondWith(getResponseFromCacheLast(cacheName, event.request));
    }
  }
});

// get the filelist, a list of every file the website content
function loadfilelist() {
  return fetch(IMPORT_URL+"/"+FILELIST)
    .then(response => {
      if (response && response.ok) {
        response.text().then(text => {
          precache(text);
        });
      } else {
        console.log("fetch for filelist not working, will have to use cache");
      }
    })
}
// put in cache everything from the filelist
// text is the raw filelist.txt content
function precache(text) {
  console.log(text);
  let linearray=text.split('\n');
  caches.open(CONTENT_CACHE).then(cache => {
    // each line contains a filename
    for (const line of linearray) {
      let [checksum, filename]=line.split("\t", 2);
      /*if (filename && filename.startsWith('./')) {
        filename=filename.substring(2);
      }*/
      if (filename && filename.startsWith("/"))  {
        console.log('low priority cache fill with: '+filename);
        cache.add(new Request(IMPORT_URL+filename, {importance: "low"}));
      }
    }
  });
}

// called when the website is initialized (not on soft refresh, neither hard refresh because they ignore workers?)
/* eslint-disable-next-line no-restricted-globals */
self.addEventListener('activate', function(e) {
  console.log('service worker: Activate');
  loadfilelist();
  /* eslint-disable-next-line no-restricted-globals */
  return self.clients.claim();
});
