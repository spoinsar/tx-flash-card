import leitnitManager from './leitnitManager';
const convert = require('xml-js');

const INT = {
	children: {
		'sc:item': {
			children: {
				'op:mcqSur': {
					children: {
						'sc:question': {
							children: {
								'op:res': {
									children: {
										'sp:txt': {
											children: {
												'op:txt': {
													children: {
														'sc:para': {
															get: true,
															name: 'title',
														},
													},
												},
											},
										},
										'sp:res': {
											get: true,
											name: 'image',
										},
									},
								},
							},
						},
						'sc:choices': {
							get: true,
							name: 'choices',
							children: {
								'sc:choice': {
									children: {
										'sc:choiceLabel': {
											children: {
												'op:txt': {
													children: {
														'sc:para': {
															get: true,
															name: 'title',
														},
													},
												},
											},
										},
									},
								},
							},
						},
						'sc:solution': {
							getAttributes: ['choice'],
							get: true,
							name: 'solution',
						},
						'sc:globalExplanation': {
							get: true,
							name: 'globalExplanation',
							children: {
								'op:res': {
									children: {
										'sp:txt': {
											children: {
												'op:txt': {
													get: true,
													name: 'details',
													children: {
														'sc:para': {
															get: true,
															name: 'details',
														},
													},
												},
											},
										},
										'sp:res': {
											get: true,
											name: 'image',
										},
									},
								},
							},
						},
					},
				},
				'op:mcqMur': {
					children: {
						'sc:question': {
							children: {
								'op:res': {
									children: {
										'sp:txt': {
											children: {
												'op:txt': {
													children: {
														'sc:para': {
															get: true,
															name: 'title',
														},
													},
												},
											},
										},
										'sp:res': {
											get: true,
											name: 'image',
										},
									},
								},
							},
						},
						'sc:choices': {
							get: true,
							name: 'choices',
							children: {
								'sc:choice': {
									getAttributes: ['solution'],
									name:'choice',
									children: {
										'sc:choiceLabel': {
											children: {
												'op:txt': {
													children: {
														'sc:para': {
															get: true,
															name: 'title',
														},
													},
												},
											},
										},
									},
								},
							},
						},
						'sc:globalExplanation': {
							get: true,
							name: 'globalExplanation',
							children: {
								'op:res': {
									children: {
										'sp:txt': {
											children: {
												'op:txt': {
													get: true,
													name: 'details',
													children: {
														'sc:para': {
															get: true,
															name: 'details',
														},
													},
												},
											},
										},
										'sp:res': {
											get: true,
											name: 'image',
										},
									},
								},
							},
						},
					},
				},
			},
		},
	},
};

const cacheImage = (img, link) => {
	const CONTENT_CACHE = "offlinewebsite-content";
	const IMPORT_URL = link + "/&/" + img + "/" + img.replace(/^.*[\\/]/, '')

	caches.open(CONTENT_CACHE).then(cache => {
        console.log('low priority cache fill with: '+img);
        cache.add(new Request(IMPORT_URL, {importance: "low"}));
	});
}

/**
 * 
 * @param {Object} interface
 * @returns {(obj: Object) => Object} function which extract the attributes from the obj parameter following the interface 
 */
const extractFromObject = (int) => (obj, link) => {
	if (obj && obj.attributes && obj.attributes["sc:refUri"]) cacheImage(obj.attributes["sc:refUri"], link);
	if (Object.keys(int).length > 1) {
		let res = {};
		if (int.getAttributes) {
			res = {
				attributes: int.getAttributes.reduce((acc, attr) => {
					if(obj.attributes[attr]) acc[attr] = obj.attributes[attr];
					return acc;
				}, {}),
				name: int.name,
			};
		}
		res.name = int.name;
		if (int.children) {
			res.elements = Object.keys(int.children).reduce((acc, key) => {
				const child = obj.elements.filter(c => c.name === key);
				if (child) acc.push(child.map(extractFromObject(int.children[key])));
				return acc;
			}, []);
		} else {
			res = {...obj, ...res};
		}
		return res;
	} else if (int.children) {
		const res = Object.keys(int.children).reduce((acc, key) => {
			const child = obj.elements.filter(c => c.name === key);
			child.forEach(c => acc.push(extractFromObject(int.children[key])(c, link)));
			return acc;
		}, []);
		return res.length > 1 ? res : res[0];
	}
	return null;
}

/**
 * 
 * @param {(qcm: Array) => void} updateGlobalQCMS 
 */
const importQCMs = async (updateGlobalQCMS, link, isCacheFirst) => {
	const test = await new Promise((resolve, reject) => {
		fetch(link+'/filelist.txt')
			.then(response => {
				if (response && response.ok) {
		        	response.text().then(text => {
		          		let linearray=text.split('\n');
		          		const promises = linearray.map(line => new Promise((resolve) => {
				    		let [checksum, filename, category, subcategory, subsubcategory]=line.split("\t", 5);
				    		if (filename && filename.startsWith("/")) {
						    	fetch(link+filename+"?isCacheFirst="+isCacheFirst)
						    		.then(response => {
						    			if (response && response.ok) {
						    				response.text().then(text => {
								    			resolve(
													[JSON.parse(convert.xml2json(text, {compact: false, ignoreComment: true, spaces: 4})), checksum, category, subcategory ? subcategory : null, subsubcategory ? subsubcategory : null]
												);
											});
							    		}
					    			});
					    	} else {
					    		console.log("Cannot read file " + filename)
					    		resolve();
					    	}
		          		}))
		          		Promise.all(promises).then(entries => {
							resolve(entries);
						});

			        });
		     	} else {
			        console.log("fetch for filelist not working, will have to use cache");
			    }
			})
			.catch(err => {
				reject(err);
			})
	});
	let res = test.filter(v => v && v[0]).map((v) => {
		if (!v || !v[0]) {return [];}
		let q = extractFromObject(INT)(v[0], link).reduce((acc, row) => {
			if(row){
				acc.push(row);
			}
			return acc;
		}, []);
		q.push({name: "checksum", value: v[1]});
		q.push({name: "category", value: v[2]});
		if (v[3]) {
			q.push({name: "subcategory", value: v[3]});
			if (v[4]) {
				q.push({name: "subsubcategory", value: v[4]});
			}
		}
		return (q);
	});
	if (res.length) {
		res = new leitnitManager(res);
  		localStorage.setItem('theme', JSON.stringify([]));
		updateGlobalQCMS(res);
  		localStorage.setItem('url', link);
	} else {
		updateGlobalQCMS();
	}
	
}

export default importQCMs;