import { useGlobal } from 'reactn';
const STORAGE_KEY = 'answerHistory';

function getNbMilliseconds(type, nb) {
  switch(type) {
    case 'sec':
      return nb * 1000;
    case 'min':
      return 60 * getNbMilliseconds('sec', nb);
    case 'hour':
      return 60 * getNbMilliseconds('min', nb);
    case 'day':
      return 24 * getNbMilliseconds('hour', nb);
    case 'week':
      return 7 * getNbMilliseconds('day', nb);
    default:
      return 0;
  }
}

/**
 * 
 * @param {[{success: Boolean, date: Date, checksum: String, url: String, category: String, subcategory: String, subsubcategory: String}]} passed 
 */
function AnswerHistoryManager(passed = []) {
  this.passed = passed;
}
AnswerHistoryManager.prototype.put = function(history) {
  const old = this.passed.findIndex(element => element.checksum === history.checksum);
  if (old === -1) {
    this.passed.push(history);
  } else {
    this.passed[old] = history;
  }
  localStorage.setItem(STORAGE_KEY, JSON.stringify(this.passed));
  return this;
}
AnswerHistoryManager.prototype.getFrom = 
/**
 * 
 * @param {'sec' | 'min' | 'hour' | 'day' | 'week'} type 
 * @param {*} nb : number to multiply 
 * @returns the answers from [nb] [type] ago to now
 * @example getFrom('min', 2) => all the answers from 2 min ago to now
 */
function(type, nb) {
  return this.passed.filter(elm => {
    const date = new Date(elm.date);
    const milli = getNbMilliseconds(type, nb);
    const dateDiff = parseInt((new Date()) - date);
    return dateDiff < milli;
  });
}
AnswerHistoryManager.prototype.load = function(updateGlobal) {
  this.passed = JSON.parse(localStorage.getItem(STORAGE_KEY)) || [];
  updateGlobal(this.passed);
  return this;
}

const answerHistoryManager = new AnswerHistoryManager();

export function useAnswerHistoryManager({ type = 'sec', nb = 0 }) {
  const updateHistory = useGlobal('answerHistory')[1];
  /**
   * 
   * @param {{success: Boolean, date: Date, checksum: String, url: String, category: String, subcategory: String, subsubcategory: String}} history
   */
  function addAnswerHistory(history) {
    answerHistoryManager.put(history);
    updateHistory([...answerHistoryManager.passed]);
  }
  return [answerHistoryManager.getFrom(type, nb), addAnswerHistory];
}

export default answerHistoryManager;