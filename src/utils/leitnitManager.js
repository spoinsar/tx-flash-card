function LeitnitManager(qcms) {
  if (Array.isArray(qcms)) {
    this._qcms = {
      '1': new LeitnitUnit(qcms),
      '2': new LeitnitUnit([]),
      '5': new LeitnitUnit([]),
    };
  } else {
    // It's from LocalStorage
    this._qcms = {
      '1': new LeitnitUnit(qcms['1']._qcms),
      '2': new LeitnitUnit(qcms['2']._qcms),
      '5': new LeitnitUnit(qcms['5']._qcms),
    };
  }
  this.current = {};
};
LeitnitManager.prototype.pick = function() {
  const keys = Object.keys(this._qcms);
  const theme = JSON.parse(localStorage.getItem('theme'));
  let totalsize = 0;
  let rnd;
  let pick;
  let key;
  let filteredQCMS;
  for(key of keys) {
    filteredQCMS = theme && theme[0] ? 
      theme[1] ? 
        theme[2] ? 
          this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1] && e.find(q => q.name === "subsubcategory")?.value === theme[2]) : 
          this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1]) : 
        this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0]) : 
      this._qcms[key]._qcms;
    totalsize += filteredQCMS.length / parseInt(key);
  }
  do {
    rnd = Math.random();
    let keyTotal = 0;
    for(key of keys) {
      filteredQCMS = theme && theme[0] ? 
        theme[1] ? 
          theme[2] ? 
            this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1] && e.find(q => q.name === "subsubcategory")?.value === theme[2]) : 
            this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1]) : 
          this._qcms[key]._qcms.filter(e => e.find(q => q.name === "category")?.value === theme[0]) : 
        this._qcms[key]._qcms;
      keyTotal += filteredQCMS.length / parseInt(key);
      if(keyTotal / totalsize >= rnd && filteredQCMS[0]) {
        pick = [filteredQCMS[0]];
        break;
      }
    }
  } while(!pick);
  this.current = {
    pick,
    key, 
  }
  localStorage.setItem(localStorage.getItem('url'), JSON.stringify(this));
  return this;
};
LeitnitManager.prototype.push = function() {
  const keys = Object.keys(this._qcms);
  const index = keys.indexOf(this.current.key);
  this._qcms[keys[index]].pull();
  if (index + 1 !== keys.length) {
    this._qcms[keys[index + 1]].push(this.current.pick[0]);
  } else {
    this._qcms[keys[index]].push(this.current.pick[0]);
  }
  this.current = {};
  localStorage.setItem(localStorage.getItem('url'), JSON.stringify(this));
  return this;
};
LeitnitManager.prototype.pull = function() {
  const keys = Object.keys(this._qcms);
  const index = keys.indexOf(this.current.key);
  this._qcms[keys[index]].pull();
  if (index !== 0) {
    this._qcms[keys[index - 1]].push(this.current.pick[0]);
  } else {
    this._qcms[keys[index]].push(this.current.pick[0]);
  }
  this.current = {};
  localStorage.setItem(localStorage.getItem('url'), JSON.stringify(this));
  return this;
};
LeitnitManager.prototype.reset = function() {
  this._qcms[this.current.key].putFront(this.current.pick[0]);
  this.current = {};
  return this;
};

function LeitnitUnit(qcms) {
  this._qcms = [...qcms];
}
LeitnitUnit.prototype.push = function(qcm) {
  this._qcms.push(qcm);
  return this;
}
LeitnitUnit.prototype.pull = function() {
  const theme = JSON.parse(localStorage.getItem('theme'));
  return this._qcms.length > 0 ? 
    theme && theme[0] ? 
      theme[1] ? 
        theme[2] ? 
          this._qcms.splice(this._qcms.findIndex(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1] && e.find(q => q.name === "subsubcategory")?.value === theme[2]), 1) : 
          this._qcms.splice(this._qcms.findIndex(e => e.find(q => q.name === "category")?.value === theme[0] && e.find(q => q.name === "subcategory")?.value === theme[1]), 1) : 
        this._qcms.splice(this._qcms.findIndex(e => e.find(q => q.name === "category")?.value === theme[0]), 1) : 
      this._qcms.splice(0, 1) : 
    null;
}
LeitnitUnit.prototype.putFront = function(qcm) {
  this._qcms = [...qcm, ...this._qcms];
  return this;
}


export default LeitnitManager;