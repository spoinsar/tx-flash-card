import joinTextWithLatex from './joinTextWithLatex';
import React from 'react';

const extractGlobalExplaination = (input) => {
  let res = null;
  if (input) {
    if (input.type === 'text') {
      return (<>{input.text + " "}</>);
    }
    if (input.type === 'element' && input.attributes && input.attributes.role === 'url') {
      return (<><a target="_blank" href={input.elements[0].elements[0].elements[0].text}>{input.elements[1].text + " "}</a></>);
    }
    if (input.type === 'element' && input.name === "image") {
      const url = localStorage.getItem('url');
      return (<img src={url + "/&/" + input.attributes['sc:refUri'] + "/" + input.attributes['sc:refUri'].replace(/^.*[\\/]/, '')} alt=""></img>);
    }
    if (input.type === 'element' && input.attributes && (input.attributes.role === 'mathtex' || input.attributes.role === 'exp' || input.attributes.role === 'ind' || input.attributes.role === 'emp')) {
      return (joinTextWithLatex({elements: [input]}));
    }
    if (input.elements) {
      res = input.elements.map( e => {
        return extractGlobalExplaination(e);
      });
      return (<>{res}</>);
    }
    if (Array.isArray(input)) {
      res = input.map( e => {
        return [extractGlobalExplaination(e), <br/>];
      });
      return (<>{res}</>);
    }
  return (<>{res}</>);
  }
}

export default extractGlobalExplaination;