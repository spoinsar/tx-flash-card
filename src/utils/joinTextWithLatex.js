import React from 'react';
import { MathJax, MathJaxContext } from 'better-react-mathjax';

/**
 * 
 * @param {[String | {  }]} text 
 * @param {[{ _attributes: {role: String}, _text: String }]} latex 
 * @returns {JSX}
 */
const joinTextWithLatex = (text) => {
  const config = {
    loader: { load: ["[tex]/html"] },
    tex: {
      packages: { "[+]": ["html"] },
      inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
      ],
      displayMath: [
        ["$$", "$$"],
        ["\\[", "\\]"]
      ]
    }
  };
  return (
    <MathJaxContext src="./mathjax/es5/tex-chtml.js" version={3} config={config}>
      <>
        {text.elements.map(t => {
          if (t.type === 'text') {
            return <>{t.text}</>;
          }
          if (t.name === 'image') {
            return (<img src={t.attributes['sc:refUri']} alt=""></img>);
          }
          let formula;
          if (t.attributes) {
            const { attributes: {role}, elements: [{text = ''} = {}] = [] } = t;
            switch(role) {
              case 'mathtex': 
                formula = `$${text}$`;
                break;
              case 'exp':
                formula = text;
                break;
              case 'ind':
                formula = text;
                break;
              case 'emp':
                formula = joinTextWithLatex(t);
                return <b>{formula}</b>;
              case 'ico':
                formula = ' [image non disponible] '; //provisoire, changer par une gestion d'images
                console.error('image non disponible ', text);
                return formula;
              default:
                formula = text;
                console.error('Mathtext role not recognized ! ', role);
            }
          }
          return <MathJax inline>{formula}</MathJax>;
        })}
      </>
    </MathJaxContext>
  );
}

/**
 * 
 * @param {String} text 
 * @param {{ _attributes: {role: String}, _text: String }} latex 
 * @returns {JSX}
 */
export const extractLatexIntoJsx = ({ attributes: {role}, elements: [{text}] }) => () =>{
  let formula;
  switch(role) {
    case 'mathtex': 
      formula = text;
      break;
    case 'exp':
      formula = `^{${text}}`;
      break;
    case 'ind':
      formula = `_{${text}}`;
      break;  
    default:
      formula = text;
      console.error('Mathtext role not recognized ! ', role);
  }
  return <MathJax inline>{formula}</MathJax>;
}

export default joinTextWithLatex;