import React, { useState, useEffect, useGlobal } from 'reactn';
import screens from './screens';
import LeitnitManager from './utils/leitnitManager';
import answerHistoryManager from './utils/answerHistoryManager';

function App() {
  // Verify window is available
  const [ loading, updateLoading ] = useState(!!window);
  const [ width, updateWidth ] = useState(!loading && window.width);
  (new Promise((resolve) => {while(!window){};resolve(false)})).then(() => {updateLoading(false);updateWidth(window.innerWidth)});
  const updateAnswerHistory = useGlobal('answerHistory')[1];
  useEffect(() => {
    function resize () {
      updateWidth(window.innerWidth);
    }
    window.addEventListener('resize', resize);
    answerHistoryManager.load(updateAnswerHistory);
    return () => { window.removeEventListener('resize', resize) }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  let [url, updateUrl] = useGlobal('url');
  if (!url) {
    url = localStorage.getItem('url');
    if (url) updateUrl(url);
  }
  let [qcms, updateQCMS] = useGlobal(url);
  if (!qcms) {
    qcms = JSON.parse(localStorage.getItem(url));
    if (qcms) updateQCMS(new LeitnitManager(qcms._qcms));
  }
  let [theme, updateTheme] = useGlobal('theme');
  if (!theme) {
    theme = JSON.parse(localStorage.getItem('theme'));
    if (theme) updateTheme(theme);
  }
  return (
    <div className="App">
      {loading ? (
        <div>Loading the application</div>
      ) : (
        <React.Fragment>
          {width < 1000 ? (
            <screens.mobile />
          ) : (
            <screens.desktop />
          )}
        </React.Fragment> 
      )}
    </div>
  );
}

export default App;
