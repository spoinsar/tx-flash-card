import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import AnimatedSwitch from './screens';

import Background from './components/Background';

const Mobile = () => {
  const baseURL = process.env.REACT_APP_BASE_URL || '';
  return (
    <React.Fragment>
      <Background />
      <Router basename={baseURL}>
        <AnimatedSwitch />
      </Router>
    </React.Fragment>
  );
}

export default Mobile;
