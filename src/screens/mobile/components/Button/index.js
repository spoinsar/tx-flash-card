import React from 'react';
import './index.css';

const Button = ({ text, disabled, onClick }) => {
  return (
    <div 
      className={`button${disabled ? '--disabled' : ''}`}
      onClick={onClick || console.error}
    >
      {text}
    </div>
  );
}

export default Button;