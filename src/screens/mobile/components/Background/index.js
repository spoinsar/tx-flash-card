import React from 'react';
import { circleStyle, circlesSVG } from './const';
import './index.css';

const Background = () => {
  return (
    <div className="background-container">
      <svg width={window.innerWidth} height={window.innerHeight} viewBox="0 0 414 896" xmlns="http://www.w3.org/2000/svg">
        {circlesSVG.map((circle, index) => (
          <circle {...circle} {...circleStyle} key={index} />
        ))}
      </svg>
    </div>
  );
}

export default Background;