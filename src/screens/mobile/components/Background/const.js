const circleStyle = {
  fill: "#4BE5FF",
  fillOpacity: "0.1",
};

const circles = [
  {
    d: 159,
    x: -54,
    y: -45,
  },
  {
    d: 211,
    x: 153,
    y: 62,
  },
  {
    d: 81,
    x: 265,
    y: 310,
  },
  {
    d: 81,
    x: 72,
    y: 448,
  },
  {
    d: 136,
    x: 322,
    y: 428,
  },
  {
    d: 199,
    x: 165,
    y: 568,
  },
  {
    d: 92,
    x: 26,
    y: 767,
  },
];

const circlesSVG = circles.map(c => ({
   r: c.d / 2,
   cx: c.x + c.d / 2,
   cy: c.y + c.d / 2,
}));

export { circleStyle, circles, circlesSVG };
export default { 
  circles,
  circleStyle,
  circlesSVG,
};