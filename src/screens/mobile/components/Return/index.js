import React from 'react';
import { useHistory } from 'react-router-dom';

const Return = ({
  beforeBack = () => null,
}) => {
  const history = useHistory();
  return (
    <img 
      alt="back" 
      src="Back.png"
      style={{ position: 'absolute', zIndex: 99 }}
      onClick={() => {
        beforeBack();
        history.goBack();
      }}
    />
  );
}

export default Return;