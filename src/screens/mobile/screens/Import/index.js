import React, { useState } from 'reactn';
import { useHistory } from 'react-router-dom';
import './index.css';
import importQCMs from '../../../../utils/xmlDataInterface';
import QrReader from 'react-qr-reader'

const Import = () => {
  let history = useHistory();
  const [link, updateLink] = useState('http://localhost:3000/questions');
  const [loading, updateLoading] = useState(false);
  const handleScan = data => {
    if (data) {
        updateLink(data);
        onClick(link);
        updateLoading(true);
    }
  }
  const handleError = err => {
    console.error(err)
  }
  const onClick = async (path) => {
    await importQCMs((qcms) => {
      if (qcms) {
        localStorage.setItem(path, JSON.stringify(qcms));
      } else {
        window.alert("Import raté, vérifiez l'URL fournie");
        console.log("No qcms found at this url");
      }
      updateLoading(false);
      updateLink('');
      history.push('home');
      window.location.reload();
    }, path, false).catch((err) => {
      console.log(err);
      window.alert("Import raté, vérifiez l'URL fournie");
      updateLoading(false);
    });
  };
  const theme = new URL(window.location.href).searchParams.get('theme');
  const [themeChanged, changeTheme] = useState(false);
  if (theme && themeChanged === false) {
    changeTheme(true);
    onClick(theme);
  }
  return (
    <div className="desktop-import-container">
      {loading && (
        <div className="desktop-import-container-loading">
          <div className="spinning-loader"/>
        </div>
      )}
      {!loading && (
      <QrReader
        delay={300}
        onError={handleError}
        onScan={handleScan}
        style={{ width: '50%' }}
      />
      )}
      <form onSubmit={(e) => {
        e.preventDefault();
        onClick(link);
        updateLoading(true);
      }}>
        <input type="text" value={link} onChange={(e) => updateLink(e.target.value)} placeholder="http://localhost:3000/questions"/>
        <input type="submit" value="Import"></input>
      </form>
    </div>
  );
}

export default Import;