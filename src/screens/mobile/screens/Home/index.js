import React, {useGlobal} from 'reactn';
import { useHistory } from 'react-router-dom';
import './index.css';

import Button from '../../components/Button';
import Statistics from '../Statistics';

export default () => {
  let history = useHistory();
  const [url] = useGlobal('url');
  let [qcms] = url ? useGlobal(url) : [];
  return (
    <div className="home-container">
      <div className="home-main">
        <div className="home-main-icon">
          <img src="icone.png" alt="Icone"/>
        </div>
        <div className="home-main-welcome">
          Bienvenue sur l'application FlashCard
        </div>
        <Button 
          text="Lancer le quiz" 
          disabled={!qcms} 
          onClick={qcms ? 
            () => history.push('qcm')
            :
            () => {}
          }
        />
        {!qcms ? (
          <div className="home-main-disabled">
            Oups, il semblerait que vous n'ayiez pas de QCM téléchargés
          </div>
        ) : (
          <Statistics />
        )}
      </div>
      <div className="home-import">
        <Button text="Import" onClick={() => { history.push('import') }}/>
      </div>
    </div>
  );
}