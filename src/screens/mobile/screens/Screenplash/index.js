import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import './index.css';

const Screenplash = () => {
  let history = useHistory();
  const [loading, updateLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      updateLoading(false);
      history.push('home'); // TODO : Do not call on desktop
    }, 3000);
  }, [history]);
  return (
    <div className="screenplash">
      <img src="icone.png" alt="Icone"/>
      {loading && <div className="spinning-loader"></div>}
    </div>
  );
};

export default Screenplash;