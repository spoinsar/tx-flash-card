import React, { useGlobal, useState } from 'reactn';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { v4 as uuidv4 } from 'uuid';

import './index.css';
import QRU from './components/QRU';
import QRM from './components/QRM';
import joinTextWithLatex from '../../../../utils/joinTextWithLatex';
import Button from '../../components/Button';
import Return from '../../components/Return';
import { useAnswerHistoryManager } from '../../../../utils/answerHistoryManager';

const QCM = () => {
  const [state, updateState] = useState(0);
  const [isTrue, updateIsTrue] = useState(false);
  const [url] = useGlobal('url');
  const [qcms] = useGlobal(url);
  const [current, updateCurrent] = useState(qcms.current.pick ? qcms.current : qcms.pick().current);
  const [uuid, updateUuid] = useState(uuidv4());
  const addAnswerHistory = useAnswerHistoryManager({})[1];
  const qcm = current.pick[0];
  const isQRU = qcm[2] && qcm[2].name === 'solution';
  return (
    <>
      {/*<Return beforeBack={() => qcms.reset()}/>*/}
      <Return/>
      <TransitionGroup style={{ width: '100%', height: '100%' }}>
        <CSSTransition classNames="qcm-container" key={uuid} timeout={200}>
          <div className="qcm-container">
            <div 
              className="qcm-title"
            style={{ top: state === 1 ? '2rem' : 'calc(2rem + 5%)', height: state === 1 ? '5vh' : '20vh', 'max-height': '20vh' }}
            >
              {qcm[0].elements ? joinTextWithLatex(qcm[0]) : qcm[0].map(q => q.elements ? q.name === "image" ? <img src={url + "/&/" + q.attributes['sc:refUri'] + "/" + q.attributes['sc:refUri'].replace(/^.*[\\/]/, '')} alt=""></img> : [joinTextWithLatex(q), <br/>] : q.map(q => [joinTextWithLatex(q), <br/>]))}
            </div>
            {isQRU ? (
              <QRU 
                state={state} 
                choices={Array.isArray(qcm[1].elements[0]) ? qcm[1].elements[0] : [qcm[1].elements[0]]} 
                solution={parseInt(qcm[2].attributes.choice)} 
                isTrue={updateIsTrue}
                globalExplanation={qcm.find(q => q.name === 'globalExplanation')}
              />
            ) : (
              <QRM 
                state={state} 
                choices={Array.isArray(qcm[1].elements[0]) ? qcm[1].elements[0] : [qcm[1].elements[0]]} 
                isTrue={updateIsTrue}
                globalExplanation={qcm.find(q => q.name === 'globalExplanation')}
              />
            )}
            <div className="qcm-validation" style={{ transform: state === 1 ? 'translateY(0%)' : 'translateY(-2500%)', color: isTrue ? 'lightgreen' : 'red' }}>
              <label>{isTrue ? "Réponse juste !" : "Réponse erronée !"}</label>
            </div>
            <div className="qcm-button">
              <Button 
                text={state === 0 ? 'Valider' : 'Continuer'} 
                onClick={() => {
                  if(state === 1){
                    const checksum = qcm.find(q => q.name === 'checksum');
                    const category = qcm.find(q => q.name === 'category');
                    const subcategory = qcm.find(q => q.name === 'subcategory');
                    const subsubcategory = qcm.find(q => q.name === 'subsubcategory');
                    addAnswerHistory({ success: isTrue,
                      date: new Date(),
                      checksum: checksum.value,
                      url: url,
                      category: category.value,
                      subcategory: subcategory ? subcategory.value : null,
                      subsubcategory: subsubcategory ? subsubcategory.value : null});
                    isTrue ? qcms.push() : qcms.pull();
                    updateCurrent(qcms.pick().current);
                    updateUuid(uuidv4());
                  }
                  updateState((state + 1) % 2);
                }} 
              />
            </div>
          </div>
        </CSSTransition>
      </TransitionGroup>
    </>
  );
}
export default QCM;