import React, { useState, useEffect } from 'react';
import './index.css';
import Radio from '../Radio';
import joinTextWithLatex from '../../../../../../utils/joinTextWithLatex';
import extractGlobalExplaination from '../../../../../../utils/extractGlobalExplaination';
import { useLocation } from "react-router-dom";

const checkIfTrue = (solution, checked) => {
  for (let i = 0; i < checked.length; i++) {
    if (!((checked[i] && solution === i + 1) || (!checked[i] && solution !== i + 1)))
      return false;
  }
  return true;
}

const QRU = ({ choices, solution, state=0, globalExplanation, isTrue = () => null }) => {
  const [checked, updateChecked] = useState(choices.map(() => false));
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  useEffect(() => {
    isTrue(checkIfTrue(solution, checked));
  }, [checked, isTrue, solution]);
  return (
    <>
      <div 
        className="qcm-question-form"
        style={{ transform: state === 1 ? 'translateY(140%)' : 'translateY(0)' }}
      >
        <form className="qcm-form">
          {choices.map((c, i) => (
            <div key={i} onClick={() => {
              updateChecked(choices.map((ch, ind) => ind === i));
            }}>
              <input 
                className="qcm-radio"
                type="radio"
                name="answer"
                value={i}
                readOnly="readonly"
                checked={checked[i]}
              />
              <label htmlFor="answer#1">{joinTextWithLatex(c)}</label>
            </div>
          ))}
        </form>
      </div>
      <div 
        className="qcm-question-answer"
        style={{ transform: state === 1 ? 'translateY(0)' : 'translateY(-140%)' }}
      >
        <div className="qcm-title">{extractGlobalExplaination(globalExplanation)}</div>
        <form className="qcm-form">
          {choices.map((c, i) => (
            <div key={i}>
              <Radio 
                value={i}
                checked={checked[i]}
                neutral={true}
                //right={((checked[i] && solution === i + 1) || (!checked[i] && solution !== i + 1))}
                onChange={() => updateChecked(choices.map((ch, ind) => ind === i))}
                name="answer-mirror"
              />
              {/*<Radio 
                value={i}
                checked={solution === i + 1}
                right={((checked[i] && solution === i + 1) || (!checked[i] && solution !== i + 1))}
                onChange={() => updateChecked(choices.map((ch, ind) => ind === i))}
              />*/}
              <label htmlFor="answer#1">{joinTextWithLatex(c)}</label>
              {(checked[i] && solution === i + 1) || (!checked[i] && solution !== i + 1) ? (!checked[i] && solution !== i + 1) && params.get("testversion") ? <label></label> : <label style={{"color":"greenyellow", "font-size":"3rem"}}> ✔ </label> : <label style={{"color":"red", "font-size":"3rem"}}> ✘ </label>}
            </div>
          ))}
        </form>
      </div>
    </>
  );
}

export default QRU;