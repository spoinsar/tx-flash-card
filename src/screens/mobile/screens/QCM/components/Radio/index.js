import React from 'react';
import './index.css';

const Radio = ({ right, neutral, ...props }) => {
  return (
    <div className={`custom-radio ${(right && 'custom-radio--right') || ''} ${(neutral && 'custom-radio--neutral') || ''}`}>
      <input type="radio" id="answer#1" name="answer" {...props} disabled/>
      <span className="checkmark"></span>
    </div>
  );
}

export default Radio;