import React from 'react';
import './index.css';
import { useGlobal } from 'reactn';


const Category = ({ category, isLoaded, successPercentage, nbQuestions, nbAnswers, indent }) => {
  	const colors = {bad : "#ffbdb9", medium : "#fecb98", good : "#bbeebb", none: "#d4d4d4", selected : "#e5f1f7"}
  	const [theme] = useGlobal('theme');
  	const indenter = "-----"
  	const lowestCategory = category[category.length-1];
	return (
		<>
	      <tr style={{"backgroundColor":isLoaded && theme && theme[indent - 1] === lowestCategory ? colors.selected : "none"}} onClick={() => {if (isLoaded) {localStorage.setItem('theme', JSON.stringify(category));window.location.reload()}}}>
	        <td><label>{indenter.repeat(indent) + " " + lowestCategory}</label></td>
	        <td>{isLoaded ? <label>{nbQuestions}</label> : <label></label>}</td>
	        <td><label>{nbAnswers}</label></td>
	        <td><div className="score-cell" style={{"backgroundColor":successPercentage < 50 ? colors.bad : successPercentage < 80 ? colors.medium : successPercentage <= 100 ? colors.good : colors.none}}><label>{successPercentage ? successPercentage : 0}%</label></div></td>
	      </tr>
	  	</>
	)
}

export default Category;