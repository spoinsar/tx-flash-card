import React from 'react';
import { Route } from 'react-router-dom';
import { AnimatedSwitch as TransitionSwitch } from 'react-router-transition';

import Screenplash from './Screenplash';
import Home from './Home';
import Import from './Import';
import QCM from './QCM';
import AdvancedStatistics from './AdvancedStatistics';


const url = new URL(window.location.href);
const theme = url.searchParams.get('theme');
const routes = [
  {
    path: '/home',
    Component: Home,
  },
  {
    path: '/import',
    Component: Import,
  },
  {
    path: '/qcm',
    Component: QCM,
  },
  {
    path: '/',
    Component: theme ? Import : Screenplash,
  },
  {
    path: '/Statistics',
    Component: AdvancedStatistics,
  },
];

const AnimatedSwitch = () => {
  return (
    <TransitionSwitch
      atEnter={{ offset: -100 }}
      atLeave={{ offset: -100 }}
      atActive={{ offset: 0 }}
      mapStyles={(styles) => ({
        transform: `translateX(${styles.offset}%)`,
      })}
      className="switch-wrapper"
    >
      {routes.map(screen => (
        <Route path={screen.path} exact key={screen.path}>
          <screen.Component />
        </Route>
      ))}
    </TransitionSwitch>
  );
}

export default AnimatedSwitch;