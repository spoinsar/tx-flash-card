import React from 'react';
import { Route } from 'react-router-dom';
import { AnimatedSwitch as TransitionSwitch } from 'react-router-transition';
import Home from './Home';
import AdvancedStatistics from './AdvancedStatistics';


const routes = [
  {
    path: '/home',
    Component: Home,
  },
  {
    path: '/',
    Component: Home,
  },
  {
    path: '/Statistics',
    Component: AdvancedStatistics,
  },
];

const AnimatedSwitch = () => {
  return (
    <TransitionSwitch
      atEnter={{ offset: -100 }}
      atLeave={{ offset: -100 }}
      atActive={{ offset: 0 }}
      mapStyles={(styles) => ({
        transform: `translateX(${styles.offset}%)`,
      })}
      className="switch-wrapper"
    >
      {routes.map(screen => (
        <Route path={screen.path} exact key={screen.path}>
          <screen.Component />
        </Route>
      ))}
    </TransitionSwitch>
  );
}

export default AnimatedSwitch;