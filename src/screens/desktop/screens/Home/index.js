import React, { useGlobal } from 'reactn';
import './index.css';

import Import from '../Import';
import QCM from '../QCM';
import Statistics from '../Statistics';

export default () => {
  const [url] = useGlobal('url');
  const [qcms] = url ? useGlobal(url) : [];
  return (
    <div className="desktop-home-container">
      <div className="desktop-home-left-menu">
        <Statistics />
        <div className="desktop-home-import">
          <Import />
        </div>
      </div>
      <div className="desktop-home-main">
        {qcms ? (
          <QCM />
        ) : (
          <div className="desktop-home-main-empty">Il semblerait que vous n'ayiez pas de QCM importés</div>
        )}
      </div>
    </div>
  );
}