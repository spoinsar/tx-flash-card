import React from 'react';
import { useGlobal } from 'reactn';
import { useAnswerHistoryManager } from '../../../../utils/answerHistoryManager';
import './index.css';
import Button from '../../components/Button';
import { useHistory } from 'react-router-dom';
import Category from './components/Category';
import LeitnitManager from '../../../../utils/leitnitManager';

const AdvancedStatistics = () => {
  const [ answerHistory ] = useAnswerHistoryManager({ type: 'week', nb: 1 });
  const [url] = useGlobal('url');
  const [qcms, updateQCM] = useGlobal(url);
  let qarray = []
  if (qcms && qcms._qcms) {
    Object.values(qcms._qcms).forEach(e => {
      qarray = qarray.concat(e._qcms.filter(e => e))
    });
  }
  let urlList = [];
  answerHistory.forEach(a => {
    urlList.push(a.url);
  });
  urlList.push(url);
  urlList = [...new Set(urlList)];
  const colors = {bad : "#ffbdb9", medium : "#fecb98", good : "#bbeebb", none: "#d4d4d4", selected : "#e5f1f7"}
  let history = useHistory();
  return (
    <div className="desktop-advanced-statistics-container">
      <div className="desktop-statistics-question-form">
        <form className="desktop-statistics-form">
          <table>
            <thead>
              <tr>
                <td><label>Thème</label></td>
                <td><label>Questions</label></td>
                <td><label>Réponses</label></td>
                <td><label>Score</label></td>
              </tr>
            </thead>
            <tbody>
              {urlList.map((u, i) => {
                const urlfilteredAnswered = answerHistory.filter(a => a.url === u);
                const urlfilteredSuccess = urlfilteredAnswered.filter(a => a.success);
                const urlsuccessPercentage = parseInt(urlfilteredSuccess.length/urlfilteredAnswered.length*100);
                const catList = [...new Set(u === url ? 
                  qarray.filter(e => e.find(q => q.name === "category")).map(e => {return e.find(q => q.name === "category").value}) : 
                  urlfilteredAnswered.filter(a => a.category).map(a => {return a.category}))];
                return (
                  <React.Fragment key={u}>
                    <tr style={{"backgroundColor":u === url ? colors.selected : "none"}} onClick={() => {
                      localStorage.setItem('url', u);
                      localStorage.setItem('theme', JSON.stringify([]));
                      const questions = JSON.parse(localStorage.getItem(u));
                      if (questions) updateQCM(new LeitnitManager(questions._qcms));
                      window.location.reload();
                    }}>
                      <td><label>{u}</label></td>
                      <td>{u === url ? <label>{qarray.length}</label> : <label>Questions non chargées</label>}</td>
                      <td><label>{urlfilteredAnswered.length}</label></td>
                      <td><div className="desktop-score-cell" style={{"backgroundColor":urlsuccessPercentage < 50 ? colors.bad : urlsuccessPercentage < 80 ? colors.medium : urlsuccessPercentage <= 100 ? colors.good : colors.none}}><label>{urlsuccessPercentage ? urlsuccessPercentage : 0}%</label></div></td>
                    </tr>
                    {!catList[0] ? <tr><td><label>Pas de statistiques disponibles</label></td></tr> : catList.map((c, i) => {
                      const filteredAnswered = urlfilteredAnswered.filter(a => a.category === c);
                      const filteredSuccess = filteredAnswered.filter(a => a.success);
                      const successPercentage = parseInt(filteredSuccess.length/filteredAnswered.length*100);
                      const subCatList = [...new Set(u === url ? 
                        qarray.filter(e => e.find(q => q.name === "category")?.value === c && e.find(q => q.name === "subcategory")).map(e => {return e.find(q => q.name === "subcategory").value}) : 
                        filteredAnswered.filter(a => a.subcategory).map(a => {return a.subcategory}))];
                      return (
                      <React.Fragment key={c}>
                        <Category 
                          category={[c]}
                          isLoaded={u === url}
                          successPercentage={successPercentage}
                          nbQuestions={qarray.filter(e => e.find(q => q.name === "category")?.value === c).length}
                          nbAnswers={filteredAnswered.length}
                          indent={1}
                        />
                        {subCatList.map((sc, i) => {
                          const filteredSubAnswered = filteredAnswered.filter(a => a.subcategory === sc);
                          const filteredSubSuccess = filteredSubAnswered.filter(a => a.success);
                          const successSubPercentage = parseInt(filteredSubSuccess.length/filteredSubAnswered.length*100);
                          const subSubCatList = [...new Set(u === url ? 
                            qarray.filter(e => e.find(q => q.name === "category")?.value === c && e.find(q => q.name === "subcategory")?.value === sc && e.find(q => q.name === "subsubcategory")).map(e => {return e.find(q => q.name === "subsubcategory").value}) : 
                            filteredSubAnswered.filter(a => a.subsubcategory).map(a => {return a.subsubcategory}))];
                          return (
                          <React.Fragment key={c+sc}>
                            <Category 
                              category={[c, sc]}
                              isLoaded={u === url}
                              successPercentage={successSubPercentage}
                              nbQuestions={qarray.filter(e => e.find(q => q.name === "subcategory")?.value === sc).length}
                              nbAnswers={filteredSubAnswered.length}
                              indent={2}
                            />
                            {subSubCatList.map((ssc, i) => {
                              const filteredSubSubAnswered = filteredSubAnswered.filter(a => a.subsubcategory === ssc);
                              const filteredSubSubSuccess = filteredSubSubAnswered.filter(a => a.success);
                              const successSubSubPercentage = parseInt(filteredSubSubSuccess.length/filteredSubSubAnswered.length*100);
                              return (
                              <React.Fragment key={c+sc+ssc}>
                                <Category
                                  category={[c, sc, ssc]}
                                  isLoaded={u === url}
                                  successPercentage={successSubSubPercentage}
                                  nbQuestions={qarray.filter(e => e.find(q => q.name === "subsubcategory")?.value === ssc).length}
                                  nbAnswers={filteredSubSubAnswered.length}
                                  indent={3}
                                />
                              </React.Fragment>
                              )
                            })}
                          </React.Fragment>
                          )
                        })}
                      </React.Fragment>
                    )})}
                  </React.Fragment>
                  )
              })}
            </tbody>
          </table>
        </form>
      </div>
      <div className ="desktop-advanced-statistics-buttons">
        <div className="desktop-home-button">
          <Button 
            text={'Désélectionner'} onClick={() => {localStorage.setItem('theme', JSON.stringify([])); window.location.reload()}}
          />
        </div>
        <div className="desktop-home-button">
          <Button 
            text={'Menu principal'} onClick={() => { history.push('home') }}
          />
        </div>
      </div>
    </div>
  );
}

export default AdvancedStatistics;