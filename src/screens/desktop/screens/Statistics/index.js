import React from 'react';
import { useGlobal } from 'reactn';
import { PieChart } from 'react-minimal-pie-chart';
import { useAnswerHistoryManager } from '../../../../utils/answerHistoryManager';
import Button from '../../components/Button';
import { useHistory } from 'react-router-dom';

import './index.css';

const Statistics = () => {
  const [ theme ] = useGlobal('theme');
  const [ url ] = useGlobal('url');
  const [ answerHistory ] = useAnswerHistoryManager({ type: 'week', nb: 1 });
  const filteredAnswered = theme && theme[0] ? theme[1] ? theme[2] ? answerHistory.filter(a => a.subsubcategory === theme[2] && a.subcategory === theme[1] && a.category === theme[0] && a.url === url) : answerHistory.filter(a => a.subcategory === theme[1] && a.category === theme[0] && a.url === url) : answerHistory.filter(a => a.category === theme[0] && a.url === url) : answerHistory.filter(a => a.url === url);
  const nbSuccess = filteredAnswered.filter(a => a.success).length;
  const nbFail = filteredAnswered.length - nbSuccess;
  let history = useHistory();
  return (
    <div className="desktop-statistics-container">
      <div className="desktop-theme-label">
        <label>Thème : {theme && theme[0] ? theme[1] ? theme[2] ? theme[2] : theme[1] : theme[0] : "Global"}</label>
      </div>
      <PieChart
        animationDuration={500}
        animationEasing="ease-out"
        center={[
          50,
          50
        ]}
        data={[
          {
            color: 'var(--color-red)',
            title: 'One',
            value: nbFail
          },
          {
            color: 'var(--color-green)',
            title: 'Two',
            value: nbSuccess
          },
        ]}
        lengthAngle={360}
        lineWidth={15}
        paddingAngle={0}
        radius={30}
        startAngle={-90}
        viewBoxSize={[
          100,
          100
        ]}
      />
      <div className="desktop-statistics-label">
        {nbSuccess} / {filteredAnswered.length}
      </div>
      <div className="desktop-statistics-button">
        <Button 
          text={'Statistiques détaillées'} onClick={() => { history.push('statistics') }}
        />
      </div>
    </div>
  );
}

export default Statistics;