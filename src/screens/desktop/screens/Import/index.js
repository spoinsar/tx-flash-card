import React, { useState } from 'reactn';
import './index.css';
import importQCMs from '../../../../utils/xmlDataInterface';

const Import = () => {
  const [link, updateLink] = useState('http://localhost:3000/questions');
  const [loading, updateLoading] = useState(false);
  const onClick = async (path) => {
    await importQCMs((qcms) => {
      if (qcms) {
        localStorage.setItem(path, JSON.stringify(qcms));
      } else {
        window.alert("Import raté, vérifiez l'URL fournie");
        console.log("No qcms found at this url");
      }
      updateLoading(false);
      updateLink('');
      window.location.reload();
    }, path, false).catch((err) => {
      console.log(err);
      window.alert("Import raté, vérifiez l'URL fournie");
      updateLoading(false);
    });
  };
  const theme = new URL(window.location.href).searchParams.get('theme');
  const [themeChanged, changeTheme] = useState(false);
  if (theme && themeChanged === false) {
    changeTheme(true);
    onClick(theme);
  }
  return (
    <div className="desktop-import-container">
      {loading && (
        <div className="desktop-import-container-loading">
          <div className="spinning-loader"/>
        </div>
      )}
      <form onSubmit={(e) => {
        e.preventDefault();
        onClick(link);
        updateLoading(true);
      }}>
        <input type="text" value={link} onChange={(e) => updateLink(e.target.value)} placeholder="http://localhost:3000/questions"/>
        <input type="submit" value="Import"></input>
      </form>
    </div>
  );
}

export default Import;