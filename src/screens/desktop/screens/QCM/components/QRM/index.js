import React, { useState , useEffect} from 'react';
import './index.css';
import Checkbox from '../Checkbox';
import joinTextWithLatex from '../../../../../../utils/joinTextWithLatex';
import extractGlobalExplaination from '../../../../../../utils/extractGlobalExplaination';
import { useLocation } from "react-router-dom";

const checkIfTrue = (choices, checked) => {
  for (let i = 0; i < checked.length; i++) {
    if (!((checked[i] && choices[i].attributes.solution === 'checked') || (!checked[i] && choices[i].attributes.solution === 'unchecked')))
      return false;
  }
  return true;
}

const QRM = ({ choices, state=0, globalExplanation, isTrue }) => {
  const [checked, updateChecked] = useState(choices.map(() => false));
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  useEffect(() => {
    isTrue(checkIfTrue(choices, checked));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checked]);
  return (
    <>
      <div 
        className="desktop-qcm-question-form"
        style={{ transform: state === 1 ? 'translateY(140%)' : 'translateY(0)' }}
      >
        <form className="desktop-qcm-form">
          {choices.map((c, i) => {
            return (
            <div key={i} onClick={() => {
              const che = checked;
              che[i] = !che[i];
              updateChecked([...che]);
            }}>
              <input 
                className="desktop-qcm-checkbox"
                type="checkbox"
                name="answer"
                value={i}
                readOnly="readonly"
                checked={checked[i]}
              />
              <label htmlFor="answer#1">{joinTextWithLatex(c.elements[0][0])}</label>
            </div>
          )})}
        </form>
      </div>
      <div 
        className="desktop-qcm-question-answer"
        style={{ transform: state === 1 ? 'translateY(0)' : 'translateY(-140%)' }}
      >
        <div className="desktop-qcm-title">{extractGlobalExplaination(globalExplanation)}</div>
        <form className="desktop-qcm-form">
          {choices.map((c, i) => (
            <div key={i}>
              <Checkbox 
                value={i}
                checked={checked[i]}
                neutral={true}
                //right={((checked[i] && choices[i].attributes.solution === 'checked') || (!checked[i] && choices[i].attributes.solution === 'unchecked'))}
                name="answer-mirror"
              />
              {/*<Checkbox 
                value={i}
                checked={choices[i].attributes.solution === 'checked'}
                right={((checked[i] && choices[i].attributes.solution === 'checked') || (!checked[i] && choices[i].attributes.solution === 'unchecked'))} 
              />*/}
              <label htmlFor="answer#1">{joinTextWithLatex(c.elements[0][0])}</label>
              {(checked[i] && choices[i].attributes.solution === 'checked') || (!checked[i] && choices[i].attributes.solution === 'unchecked') ? (!checked[i] && choices[i].attributes.solution === 'unchecked') && params.get("testversion") ? <label></label> : <label style={{"color":"greenyellow", "font-size":"3rem"}}> ✔ </label> : <label style={{"color":"red", "font-size":"3rem"}}> ✘ </label>}
            </div>
          ))}
        </form>
      </div>
    </>
  );
}

export default QRM;