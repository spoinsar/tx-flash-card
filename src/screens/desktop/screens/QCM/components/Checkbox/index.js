import React from 'react';
import './index.css';

const Checkbox = ({ right, checked, neutral, ...props }) => {
  return (
    <div className={`custom-checkbox ${(right && 'custom-checkbox--right') || ''} ${(neutral && 'custom-checkbox--neutral') || ''}`}>
      <input type="checkbox" id="answer#1" name="answer" checked={checked} {...props} disabled/>
      <span className="checkmark"></span>
    </div>
  );
}

export default Checkbox;