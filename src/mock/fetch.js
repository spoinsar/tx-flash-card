import fetch_array from './fetch.json';

const qcms = fetch_array.map(JSON.parse);

export default qcms;