const path_to_qcm = './public/qcm/chimie';

(async () => {
  const fs = require('fs');
  const convert = require('xml-js');
  const util = require('util');
  const exec = util.promisify(require('child_process').exec);
  const { stdout } = await exec(`ls ${path_to_qcm} | grep "\\.quiz"`);
  let public_links = stdout.split('\n').map(v => `./public/qcm/chimie/${v}`);
  public_links = public_links.slice(0, public_links.length - 1);
  const content = public_links.map(p => {
    let file = fs.readFileSync(p, 'utf8');
    file = convert.xml2json(file, {compact: false, ignoreComment: true, spaces: 4});
    return file;
  });
  fs.writeFileSync('./src/mock/fetch.json', JSON.stringify(content));
})();
/*
const [path_to_file_to_be_zipped, max_size] = process.argv.slice(2);

if (!path_to_file_to_be_zipped) {
  console.error('Please pass the files to be zipped, e.g node index.js /path/to/file');
  process.exit(0);
}

if (!max_size) {
  max_size = Infinity;
}

const makePromise = (fun, ...args) => new Promise((resolve, reject) => {
  fun(...args, (err, val) => {
    if (err) {
      reject(err);
    }
    resolve(val || true);
  })
})

const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

(async () => {
  if(
    await makePromise(fs.access, path_to_file_to_be_zipped, fs.constants.F_OK | fs.constants.R_OK)
  ) {
    let archive;
    if ((await makePromise(fs.stat, path_to_file_to_be_zipped)).isDirectory()) {
      const util = require('util');
      const exec = util.promisify(require('child_process').exec);
      const { stdout } = await exec(`du -xks ${path_to_file_to_be_zipped} | grep "\\${path_to_file_to_be_zipped}$"`);
      const size = parseInt(stdout.split(' ')[0]) * 1024;
      if (size < max_size) {
        archive = createArchive(path_to_file_to_be_zipped);
        archive.directory(path_to_file_to_be_zipped, false);
        archive.finalize();
      }
      else {
        const folders = (await exec(`du -xk ${path_to_file_to_be_zipped} | grep "\\${path_to_file_to_be_zipped}/[^/]*$"`)).stdout.split('\n');
        const bulks = folders.reduce((acc, folder) => {
          const folderSize = parseInt(folder.split('\t')[0]) * 1024;
          const folderPath = folder.split('\t')[1];
          if(!folderSize) return acc;
          if (acc[acc.length-1][0] + folderSize < max_size) {
            acc[acc.length-1][0] += folderSize;
            acc[acc.length-1][1].push(folderPath);
          } else {
            acc.push([folderSize, [folderPath]]);
          }
          return acc;
        }, [[0, []]]);
        bulks.forEach((val, index) => {
          archive = createArchive(path_to_file_to_be_zipped, index);
          val[1].forEach((folder) => {
            let folderName = folder.split('/');
            folderName = folderName[folderName.index - 1];
            if(fs.statSync(folder).isDirectory()) {
              archive.directory(folder, folderName);
            } else {
              archive.append(fs.createReadStream(folder), { name: folderName });
            }
          });
          archive.finalize();
        });
      }
    } else {
      archive = createArchive(path_to_file_to_be_zipped);
      archive.append(fs.createReadStream(path_to_file_to_be_zipped), { name });
      archive.finalize();
    }
  }
})();

function createArchive(filePath, number) {
  let name = filePath.split('/');
  name = name[name.length - 1];
  const zipPath = path.join(__dirname, name.split('.')[0] + (number || '') + '.zip');
  const output = fs.createWriteStream(zipPath);
  const archive = archiver('zip', {
    zlib: { level: 9 },
  });
  output.on('close', () => {
    console.log(`archive archived ${archive.pointer()}bytes`);
    console.log('archiving ended');
  })
  archive.pipe(output);
  return archive;
}
*/