Commandes pour le projet:
- npm install
- npm start

Commandes pour le serveur de mock d'import (url: http://localhost:3002/chimie):
- cd mock-server
- npm i
- npm start

Pour build l'application :
Il faudra créer un fichier appelé ".env.local" avec :
- REACT_APP_BASE_URL=/sub/path
Puis run :
- npm run build

Le build est dans le dossier build, il suffit de serve index.html (en HTTPS pour la PWA, sinon ça ne marchera pas en PWA).

Quand vous aurez les icônes, il faudra les mettre dans public à la place des logo et du favicon. Le manifest.json devra être configuré si besoin.

