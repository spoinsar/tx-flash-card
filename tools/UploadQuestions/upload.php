<?php
$user_dir = "../fcdata/" . $_SERVER['REMOTE_USER'] . "/";
$course_dir = $user_dir . $_POST['folderName'] ."/";
$target_file = $course_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$scarFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

if($scarFileType != "scar") {
  echo "Le fichier n'est pas un scar. ";
  $uploadOk = 0;
}

// Check if file already exists
if (file_exists($target_file)) {
  echo "Le fichier existe déjà. ";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
  echo "Le fichier est trop gros. ";
  $uploadOk = 0;
}

if (!file_exists($user_dir)) { 
  $old = umask(0);
  if (!mkdir($user_dir, 0777)) {
    echo "Création du dossier utilisateur impossible. ";
    $uploadOk = 0;
  } else {
    umask($old);
  }
}

if (!file_exists($course_dir)) { 
  $old = umask(0);
  if (!mkdir($course_dir, 0777)) {
    echo "Création du dossier impossible, vérifiez que le nom de dossier soit valide. ";
    $uploadOk = 0;
  } else {
    umask($old);
  }
}

function str_starts_with ( $haystack, $needle ) {
  return strpos( $haystack , $needle ) === 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Echec de l'upload";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    $zip = new ZipArchive;
    if ($zip->open($target_file) === TRUE) {
        $zip->extractTo($course_dir);
        $zip->close();
        $exportfile = glob($course_dir . "*.xml");
        if ($exportfile[0]) {
          $command = escapeshellcmd('/opt/tx-flash-card/tools/parseexport.py ' . $course_dir . " " . $exportfile[0]);
          $output = shell_exec($command);
          if (str_starts_with($output,"success")) {
            echo "Le fichier ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " a été uploadé. Le chemin d'import est : https://socles3.unisciel.fr/fcweb/?theme=https://socles3.unisciel.fr/fcdata/" . $_SERVER['REMOTE_USER'] . "/" . $_POST['folderName'];
          }
          else {
            echo "Echec de création du filelist";
          }
        } else {
          echo "Echec : Pas de fichier d'export moodle XML trouvé dans le scar.";
        }
    } else {
        echo "Echec de l'upload";
    }
    unlink($target_file);
  } else {
    echo "Echec de l'upload";
  }
}
?>