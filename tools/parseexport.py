#!/usr/bin/env python3

import xml.dom.minidom
import hashlib
import sys

args = sys.argv
dom = ""
if (len(args) == 3) :
	dom = xml.dom.minidom.parse(args[2])
	toFile = ""
	for element in dom.getElementsByTagName("sp:trainUcMcqMur"):
		path = element.getAttribute('sc:refUri')
		f = open(args[1] + path, "r")
		data = f.read()
		f.close()
		h = hashlib.new('sha256')
		h.update(data.encode("utf-8"))
		toFile += h.hexdigest()
		toFile += "\t/" + path
		if (element.parentNode.nodeName == "opa:mdlCat") :
			if (element.parentNode.parentNode.parentNode.nodeName == "opa:mdlCat") :
				if (element.parentNode.parentNode.parentNode.parentNode.parentNode.nodeName == "opa:mdlCat") :
					toFile += "\t" + str(element.parentNode.parentNode.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.nodeValue)
				toFile += "\t" + str(element.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.nodeValue)
			toFile += "\t" + str(element.parentNode.firstChild.firstChild.firstChild.nodeValue)
		toFile += "\n"
	for element in dom.getElementsByTagName("sp:trainUcMcqSur"):
		path = element.getAttribute('sc:refUri')
		f = open(args[1] + path, "r")
		data = f.read()
		f.close()
		h = hashlib.new('sha256')
		h.update(data.encode("utf-8"))
		toFile += h.hexdigest()
		toFile += "\t/" + path
		if (element.parentNode.nodeName == "opa:mdlCat") :
			if (element.parentNode.parentNode.parentNode.nodeName == "opa:mdlCat") :
				if (element.parentNode.parentNode.parentNode.parentNode.parentNode.nodeName == "opa:mdlCat") :
					toFile += "\t" + str(element.parentNode.parentNode.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.nodeValue)
				toFile += "\t" + str(element.parentNode.parentNode.parentNode.firstChild.firstChild.firstChild.nodeValue)
			toFile += "\t" + str(element.parentNode.firstChild.firstChild.firstChild.nodeValue)
		toFile += "\n"
	f = open(args[1] + "filelist.txt", "w")
	f.write(toFile)
	f.close()
	print("success");
else :
	print("Veuillez renseigner le dossier contenant les questions et le fichier d'export moodle de cette façon :")
	print("python3 parseexport.py /chemin/vers/dossier/questions/ /chemin/vers/fichier/exportmoodle.xml")
