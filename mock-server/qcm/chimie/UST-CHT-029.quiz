<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Calcul de l'Enthalpie Standard</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-thermochim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>Thermochimie</sp:keywd>
							<sp:keywd>Hess</sp:keywd>
							<sp:keywd>Enthalpie</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Calculer l'enthalpie standard de la réaction suivante à 298 K:</sc:para>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">CO_{(g)}+2H_{2\,(g)}\rightleftharpoons CH_{3}OH_{(g)}</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">Données à 298 K: <sc:textLeaf role="mathtex">\varDelta_{f}H^{0}(CO)=-110,6\,kJ/mol</sc:textLeaf> ; <sc:textLeaf role="mathtex">\varDelta_{f}H^{0}(CH_{3}OH)=-232,0\,kJ/mol</sc:textLeaf>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">36024,2 kJ/mol</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Comment faites-vous pour aboutir à un tel résultat ?</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">-342,6 kJ/mol</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Attention au signe, il faut soustraire l'enthalpie de formation des réactifs et non l'additionner.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">342,6 kJ/mol</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Apparemment vous avez cru que le fait de voir cette valeur absolue apparaître deux fois signifierait que l'une des deux réponses serait correcte.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">-121,4 kJ/mol</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="4"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Pour aller plus loin :</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_10.html</sp:url>
<sp:title>Loi de Hess</sp:title>
</op:urlM>Loi de Hess</sc:phrase>
</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_11.html</sp:url>
<sp:title>Enthalpie standard de formation à 298 K</sp:title>
</op:urlM>Enthalpie standard de formation à 298 K</sc:phrase>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
