<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Dosage par titrage</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-agir-econres-dosdir-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimexpetech-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">2</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">
							
      <sc:phrase role="url">
<op:urlM>
<sp:url>http://univ-lille1.fr</sp:url>
</op:urlM>univ-lille1.fr</sc:phrase>
    						
						</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">
			<sc:inlineStyle role="emp">Détection de l'équivalence lors de différents types de titrage</sc:inlineStyle>
		</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			Lors d'un titrage pH-métrique, l'équivalence est repérée par un saut de pH.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			Lors d'un titrage conductimétrique, l'équivalence est repérée par un saut de conductivité de la solution.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">
			Lors d'un titrage conductimétrique, l'équivalence est repérée par un changement de pente de la courbe d'évolution de la conductivité de la solution en fonction du volume versé avec la burette.
		</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			Lors d'un titrage conductimétrique, l'équivalence est repérée par un changement de couleur.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">
			L'équivalence est repérée par un changement de pente de la conductivité.
		</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			En cas d'utilisation d'un indicateur coloré de pH comme indicateur de fin de réaction, celui-ci doit être choisi de manière à ce que sa zone de virage soit située en dehors du saut de pH.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">
			Il doit être choisi de manière à ce que sa zone de virage soit située dans le saut de pH.
		</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="mar" creationTime="1433061085373" updateTime="1433151163056">Ajouter l'explication générale suivante :
Lors d'un titrage conductimétrique, l'équivalence est repérée par un changement de pente de la courbe d'évolution de la conductivité de la solution contenue dans le becher en fonction du volume versé avec la burette.
Lors d'un titrage colorimétrique, l'équivalence est repérée par un changement de couleur. En cas d'utilisation d'un indicateur coloré de pH comme indicateur de fin de réaction, celui-ci doit être choisi de manière à ce que sa zone de virage soit située dans le saut de pH.
Lors d'un titrage pH-métrique, l'équivalence est repérée par un saut de pH sur la courbe d'évolution du pH de la solution contenue dans le becher en fonction du volume versé avec la burette.

</comment></comment>-->
					<op:txt>
						<sc:para xml:space="preserve">
			<sc:inlineStyle role="emp">Compétence(s)</sc:inlineStyle>.  <sc:inlineStyle role="emp">Savoir comment détecter l'équivalence lors de titrages pH-métrique, conductimétrique et colorimétrique.</sc:inlineStyle>
		</sc:para>
						<sc:para xml:space="preserve">Un titrage par dosage est caractérisé par 3 phases : (A est l'espèce titrée et B l'espèce titrante)</sc:para>
						<sc:itemizedList>
							<sc:listItem>
								<sc:para xml:space="preserve">B est limitant. A est toujours présent dans la solution titrée.</sc:para>
							</sc:listItem>
							<sc:listItem>
								<sc:para xml:space="preserve">L'équivalence. B et A sont introduits dans des proportions stœchiométriques. Ni A ni B ne sont présents dans la solution titrée.</sc:para>
							</sc:listItem>
							<sc:listItem>
								<sc:para xml:space="preserve">A est limitant. A n'est plus présent dans la solution titrée.</sc:para>
							</sc:listItem>
						</sc:itemizedList>
						<sc:para xml:space="preserve">Lors d'un titrage, on cherche à déterminer l'équivalence :</sc:para>
						<sc:itemizedList>
							<sc:listItem>
								<sc:para xml:space="preserve">Avec un suivi conductimétrique, l'équivalence est repérée par un changement de pente de la courbe d'évolution de la conductivité de la solution contenue dans le becher en fonction du volume versé avec la burette.</sc:para>
							</sc:listItem>
							<sc:listItem>
								<sc:para xml:space="preserve">Avec un suivi  pH-métrique, l'équivalence est repérée par un saut de pH sur la courbe d'évolution du pH de la solution contenue dans le becher en fonction du volume versé avec la burette.</sc:para>
							</sc:listItem>
							<sc:listItem>
								<sc:para xml:space="preserve">Avec un suivi colorimétrique, l'équivalence est repérée par un changement de couleur. En cas d'utilisation d'un indicateur coloré de pH comme indicateur de fin de réaction, celui-ci doit être choisi de manière à ce que sa zone de virage soit située dans le saut de pH .</sc:para>
							</sc:listItem>
						</sc:itemizedList>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>
