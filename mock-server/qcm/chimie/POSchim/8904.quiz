<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Dosage spectrophotométrique</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-obscoulim-matcolo-loibeer-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimexpetech-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">1</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">
							
      <sc:phrase role="url">
<op:urlM>
<sp:url>http://univ-lille1.fr</sp:url>
</op:urlM>univ-lille1.fr</sc:phrase>
    						
						</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1519045197193">Absorbance d'une solution et loi de Beer-Lambert.</comment></comment>-->
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">
			   D'après la loi de Beer-Lambert, l’absorbance, à la longueur d’onde <sc:textLeaf role="mathtex">\lambda</sc:textLeaf>, d’une solution colorée de concentration <sc:inlineStyle role="emp">c</sc:inlineStyle> introduite dans une cuve de largeur <sc:textLeaf role="mathtex">l</sc:textLeaf>
		 est</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			proportionnelle à <sc:textLeaf role="mathtex">\lambda</sc:textLeaf>.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			proportionnelle à <sc:inlineStyle role="emp">c</sc:inlineStyle>.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			une grandeur qui s’exprime en <sc:textLeaf role="mathtex">\textrm{mol.L}^{-1}</sc:textLeaf>.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			une grandeur sans unité.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="mar" creationTime="1433060430463" updateTime="1433060514622">Ajouter le symbole l de la largueur de la cuve dans l'expression de A(λ) :
A(λ) = ε(λ).l.c
Dans la dernière phrase, ajouter "en cm," entre le symbole l et le symbole c.</comment></comment>-->
					<op:txt>
						<sc:para xml:space="preserve">
			<sc:inlineStyle role="emp">Compétence(s)</sc:inlineStyle>.  <sc:inlineStyle role="emp">Exprimer l'absorbance d'une solution selon la loi de Beer-Lambert.</sc:inlineStyle>
		</sc:para>
						<sc:para xml:space="preserve">
			D'après la loi de Beer-Lambert, l’absorbance à la longueur d’onde <sc:textLeaf role="mathtex">\lambda</sc:textLeaf>, d’une solution colorée de concentration <sc:inlineStyle role="emp">c</sc:inlineStyle> introduite dans une cuve de largeur <sc:textLeaf role="mathtex">l</sc:textLeaf>
		</sc:para>
						<sc:para xml:space="preserve">
			<sc:textLeaf role="mathtex">\textrm{A}(\lambda)=\epsilon(\lambda).l.c</sc:textLeaf>
		</sc:para>
						<sc:para xml:space="preserve">
			où <sc:textLeaf role="mathtex">\epsilon(\lambda)</sc:textLeaf> est le coefficient d’absorption molaire de l’espèce colorée à la longueur d’onde <sc:textLeaf role="mathtex">\lambda</sc:textLeaf>.
		</sc:para>
						<sc:para xml:space="preserve">
			<sc:textLeaf role="mathtex">\epsilon(\lambda)</sc:textLeaf> s'exprime en <sc:textLeaf role="mathtex">\textrm{mol}^{-1}.\textrm{L}.\textrm{cm}^{-1}</sc:textLeaf>, <sc:inlineStyle role="emp">
<sc:textLeaf role="mathtex">l</sc:textLeaf>
</sc:inlineStyle> en <sc:textLeaf role="mathtex">\textrm{cm}</sc:textLeaf>, <sc:inlineStyle role="emp">c</sc:inlineStyle> en <sc:textLeaf role="mathtex">\textrm{mol.L}^{-1}</sc:textLeaf>. Par conséquent <sc:inlineStyle role="emp">A</sc:inlineStyle> est une grandeur sans unité.
		</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>
