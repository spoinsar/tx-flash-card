<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Concentration ionique</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-comploismod-cohtransf-consmat-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimsolu-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">
							
      <sc:phrase role="url">
<op:urlM>
<sp:url>http://univ-lille1.fr</sp:url>
</op:urlM>univ-lille1.fr</sc:phrase>
    						
						</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1519047702837">Concentrations molaires des ions d'un solide ionique après dissolution.</comment></comment>-->
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="etu02" creationTime="1430815597698">Des ions Fe3+, et pas Fe2+</comment><comment author="mar" creationTime="1430983261395">c'est exact.</comment></comment>-->
					<op:txt>
						<sc:para xml:space="preserve">
			<sc:inlineStyle role="emp">  </sc:inlineStyle>Un volume de 100,00 mL d'une solution aqueuse contenant des ions Fe<sc:textLeaf role="exp">3+</sc:textLeaf> et Cl<sc:textLeaf role="exp">-</sc:textLeaf> a été préparée en dissolvant 0,10 mol de FeCl<sc:textLeaf role="ind">3</sc:textLeaf>.   Dans cette solution :
		</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			[Fe<sc:textLeaf role="exp">3+</sc:textLeaf>] = 3 [Cl<sc:textLeaf role="exp">-</sc:textLeaf>]
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">
			En se dissolvant 1 mol de FeCl<sc:textLeaf role="ind">3</sc:textLeaf> libère 1 mol d'ions Fe<sc:textLeaf role="exp">3+</sc:textLeaf> pour 3 mol d'ions Cl<sc:textLeaf role="exp">-</sc:textLeaf> donc [Cl<sc:textLeaf role="exp">-</sc:textLeaf>] = 3[Fe<sc:textLeaf role="exp">3+</sc:textLeaf>].
		</sc:para>
						<sc:para xml:space="preserve">
			Autre justification : la solution doit être électriquement neutre. De ce fait les charges positives des ions Fe<sc:textLeaf role="exp">3+</sc:textLeaf> doivent être compensées par les charges négatives des ions Cl<sc:textLeaf role="exp">-</sc:textLeaf> : elle contient donc 3 fois plus d’ions<sc:textLeaf role="exp"> </sc:textLeaf>Cl<sc:textLeaf role="exp">-</sc:textLeaf> que d’ions Fe<sc:textLeaf role="exp">3+</sc:textLeaf>.
		</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			[Cl<sc:textLeaf role="exp">-</sc:textLeaf>] = 3 [Fe<sc:textLeaf role="exp">3+</sc:textLeaf>]
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			[Fe<sc:textLeaf role="exp">3+</sc:textLeaf>] = 0,010 mol.L<sc:textLeaf role="exp">-1</sc:textLeaf>.
		</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="mar" creationTime="1433003296230">Il faut ajouter après la fraction :
= 1,0 mol.L^-1</comment></comment>-->
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="etu02" creationTime="1430815657823" updateTime="1430815770062">Il y a un -1 en trop à la fin de l'expression à la place du résultat = 1,0 mol.L-1</comment><comment author="mar" creationTime="1430983823461" updateTime="1430993076420">remplacer le -1 par = 1,0 mol.L^-1</comment><comment author="ext-lug" creationTime="1431939173678">Corrige a valider</comment></comment>-->
					<op:txt>
						<sc:para xml:space="preserve">
			En se dissolvant 1 mol de FeCl<sc:textLeaf role="ind">3</sc:textLeaf> libère 1 mol d'ions Fe<sc:textLeaf role="exp">3+</sc:textLeaf> donc <sc:inlineStyle role="emp">n</sc:inlineStyle>(Fe<sc:textLeaf role="exp">3+</sc:textLeaf>) = <sc:inlineStyle role="emp">n</sc:inlineStyle>(FeCl<sc:textLeaf role="ind">3</sc:textLeaf>).
		</sc:para>
						<sc:para xml:space="preserve">
			Il vient : <sc:textLeaf role="mathtex">\left[ \textrm{Fe}^{3+} \right] = \frac{n \left( \textrm{Fe}^{3+} \right)}{V} = \frac{0,10}{0,100}</sc:textLeaf>
<sc:textLeaf role="exp"/> = 1,0 mol.L^-1</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice solution="checked">
				<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="mar" creationTime="1433003398731">mol.L^-1 ne doit pas être écrit en italique</comment></comment>-->
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
			[Fe<sc:textLeaf role="exp">3+</sc:textLeaf>] = 1,0 <sc:textLeaf role="mathtex">mol.L^{-1}</sc:textLeaf>
<sc:textLeaf role="exp"/>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="etu02" creationTime="1430815857504">Faute d'orthographe "la quantité de matière de cet ion diviséE"</comment></comment>-->
					<op:txt>
						<sc:para xml:space="preserve">
			<sc:inlineStyle role="emp">Compétence(s)</sc:inlineStyle>.  <sc:inlineStyle role="emp">Calculer la concentration molaire des ions d'un solide ionique après dissolution.  </sc:inlineStyle>
		</sc:para>
						<sc:para xml:space="preserve">
			La concentration molaire d’un ion X en solution, notée [X], est égale à la quantité de matière de cet ion divisée par le volume de la solution. Elle se calcule à l’aide de l’expression suivante :
		</sc:para>
						<sc:para xml:space="preserve">
			<sc:textLeaf role="mathtex">[ \textrm{X}] = {\frac{\displaystyle n(\textrm{X})}{\displaystyle V}}</sc:textLeaf>
		</sc:para>
						<sc:para xml:space="preserve">
			avec <sc:inlineStyle role="emp">n</sc:inlineStyle>(X) la quantité de matière de l'ion X et <sc:inlineStyle role="emp">V</sc:inlineStyle> le volume de la solution.
		</sc:para>
						<sc:para xml:space="preserve">
			La relation entre <sc:inlineStyle role="emp">n</sc:inlineStyle>(X) et <sc:inlineStyle role="emp">n</sc:inlineStyle>(soluté), quantité de matière de soluté dissoute, se déduit de la stœchiométrie de la réaction de dissolution.
		</sc:para>
						<sc:para xml:space="preserve">
			La réaction de dissolution de FeCl<sc:textLeaf role="ind">3</sc:textLeaf> est :
		</sc:para>
						<sc:para xml:space="preserve">
			FeCl<sc:textLeaf role="ind">3</sc:textLeaf>(s) → Fe<sc:textLeaf role="exp">3+</sc:textLeaf>(aq) + 3 Cl<sc:textLeaf role="exp">-</sc:textLeaf>(aq)
		</sc:para>
						<sc:para xml:space="preserve">
			Une solution ionique est électriquement neutre.
		</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>
