<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Second principe de la thermodynamique</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLicence>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-thermochim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">4</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						<op:keywds>
							<sp:keywd>Thermochimie</sp:keywd>
							<sp:keywd>Enthalpie libre</sp:keywd>
							<sp:keywd>Enthalpie</sp:keywd>
							<sp:keywd>Entropie</sp:keywd>
							<sp:keywd>niveau difficile</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Quelle est la température d'inversion de la réaction suivante :</sc:para>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">CO_{(g)}+2\,H_{2\,(g)}\rightleftharpoons CH_{3}OH_{(g)}</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">Données à <sc:textLeaf role="mathtex">298</sc:textLeaf>K : <sc:textLeaf role="mathtex">\varDelta_{f}H^{0}(CO_{(g)})=-110,6\,kJ.mol^{-1}</sc:textLeaf> ; <sc:textLeaf role="mathtex">\varDelta_{f}H^{0}(CH_{3}OH_{(g)})=-201,2\,kJ.mol^{-1}</sc:textLeaf> ; <sc:textLeaf role="mathtex">S^{0}(CH_{3}OH_{(g)})=238,0\,J.mol^{-1}.K^{-1}</sc:textLeaf> ; <sc:textLeaf role="mathtex">S^{0}(CO_{(g)})=197,9\,J.mol^{-1}.K^{-1}</sc:textLeaf> ; <sc:textLeaf role="mathtex">S^{0}(H_{2\,(g)})=130,5\,J.mol^{-1}.K^{-1}</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">La température d'inversion est la température à partir de laquelle la réaction devient défavorable.</sc:para>
						<sc:para xml:space="preserve">On fait l'hypothèse que l'enthalpie ainsi que l'enthalpie libre de la réaction sont indépendants de la température :</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> <sc:textLeaf role="mathtex">112</sc:textLeaf> K</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Vous avez dû confondre enthalpie et enthalpie libre à la dernière étape du calcul.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">Il n'existe pas de température d'inversion</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">410</sc:textLeaf> K</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">322</sc:textLeaf> K</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Je ne vois pas comment vous avez pu aboutir à ce résultat.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="3"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Pour aller plus loin :</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_10.html</sp:url>
<sp:title>Loi de Hess</sp:title>
</op:urlM>Loi de Hess</sc:phrase>
</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_11.html</sp:url>
<sp:title>Enthalpie standard de formation à 298 K</sp:title>
</op:urlM>Enthalpie standard de formation à 298 K</sc:phrase>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
