<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Quantité de matière</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">1</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						<op:keywds>
							<sp:keywd>méthodologie</sp:keywd>
							<sp:keywd>Quantité de matière</sp:keywd>
							<sp:keywd>niveau facile</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Choisir la ou les relations permettant de déterminer la quantité de matière <sc:textLeaf role="mathtex">n</sc:textLeaf> (mole) d'atomes dans un échantillon, connaissant la masse d'un échantillon <sc:textLeaf role="mathtex">m</sc:textLeaf> (g), sa masse molaire <sc:textLeaf role="mathtex">M</sc:textLeaf> (g*mol<sc:textLeaf role="exp">-1</sc:textLeaf>) ou son nombre d'atomes <sc:textLeaf role="mathtex">N</sc:textLeaf> est :</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">Le~nombre~de~moles~n~(mol)=\frac{Masse~de~l' \acute{e}chantillon~m~(g)}{Masse~molaire~M~(g*mol^{-1})}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> <sc:textLeaf role="mathtex">Masse~de~l' \acute{e}chantillon~m~(g)=Masse~molaire~M~(g~*~mol^{-1})~\times~Le~nombre~de~moles~n~(mol)</sc:textLeaf>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">Le~nombre~de~moles~n~(mol)=\frac{Masse~molaire~M~(g*mol^{-1})}{Masse~de~l' \acute{e}chantillon~m~(g)}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">Masse~de~l' \acute{e}chantillon~m~(g)=\frac{Nombre~d'atomes~N*Masse~molaire~M~(g*mol^{-1})}{Nombre~d'Avogadro~N_A~(mol^{-1})}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">Le~nombre~de~moles~n~(mol)=\frac{Masse~de~l' \acute{e}chantillon~m~(g)}{Nombre~d'atomes~N*Nombre~d'Avogadro~N_A~(mol^{-1})}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Par définition, la relation reliant la quantité de matière <sc:textLeaf role="mathtex">n</sc:textLeaf>, la masse d'un échantillon <sc:textLeaf role="mathtex">m</sc:textLeaf>, la masse molaire <sc:textLeaf role="mathtex">M</sc:textLeaf> est : <sc:textLeaf role="mathtex">n~(mol)=\frac{m~(g)}{M~(g*mol^{-1})}</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">L'analyse dimensionnelle de la relation <sc:textLeaf role="mathtex">n~(mol)=\frac{M~(g*mol^{-1})}{m~(g)}</sc:textLeaf> donne une unité en <sc:textLeaf role="mathtex">mol^{-1}</sc:textLeaf> ce qui n'est pas cohérent.</sc:para>
						<sc:para xml:space="preserve">La relation <sc:textLeaf role="mathtex">m~(g)=M~(g~*~mol^{-1})~\times n~(mol)</sc:textLeaf> avec <sc:textLeaf role="mathtex">n=\frac{N}{N_A}</sc:textLeaf> est équivalente à : <sc:textLeaf role="mathtex">m~(g)=\frac{N*M~(g*mol^{-1})}{N_A~(mol^{-1})}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>
