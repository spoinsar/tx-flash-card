<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Équilibres chimiques</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-thermochim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1528362132653">enlever "thermochimie" car déjà dans le thème
enlever peut-être aussi "niveau moyen"</comment><comment author="ves" creationTime="1537792566600">"niveau moyen" supprimé</comment></comment>-->
						<op:keywds>
							<sp:keywd>Thermochimie</sp:keywd>
							<sp:keywd>Le Chatelier</sp:keywd>
							<sp:keywd>Équilibres chimiques</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Soit le système à l'équilibre <sc:textLeaf role="mathtex">2H_2(g) + O_2(g) \rightleftharpoons 2H_2O(g) + \acute{e}nergie</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">Que peut-on faire afin d'augmenter la concentration de <sc:textLeaf role="mathtex">H_2O(g)</sc:textLeaf> :</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">augmenter la pression</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> ajouter un catalyseur</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Un catalyseur modifie la cinétique d'une réaction mais pas la thermodynamique</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">élever la température</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Si la réaction libère de l'énergie sous forme de chaleur dans le sens direct, le principe de Le Chatelier vous assure qu'une augmentation de la température favoriserait le sens indirect</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">soutirer une quantité d'hydrogène gazeux du système</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Si la réaction libère consomme le dihydrogène dans le sens direct, le principe de Le Chatelier vous assure que le soutirer favoriserait le sens indirect</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="1"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Si on perturbe le système en augmentant la pression on déplace de l'équilibre dans le sens d'une diminution de la pression du système , c'est à dire dans le sens d'une diminution du nombre de moles de composés gazeux du système, c'est à dire que l'on favorise la formation de la molécule d'eau.</sc:para>
						<sc:para xml:space="preserve">Pour en savoir plus :</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch05/co/apprendre_ch5_07.html</sp:url>
<sp:title>Lois qualitatives de Le Chatelier</sp:title>
</op:urlM>Lois qualitatives de Le Chatelier</sc:phrase>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
