<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Second principe de la thermodynamique</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-thermochim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">4</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>Thermochimie</sp:keywd>
							<sp:keywd>Enthalpie libre</sp:keywd>
							<sp:keywd>Enthalpie</sp:keywd>
							<sp:keywd>Entropie</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Quelle est la température d'inversion de la réaction suivante :</sc:para>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">N_{2\,(g)}+3\,H_{2\,(g)}\rightleftharpoons2\,NH_{3\,(g)}</sc:textLeaf>
</sc:para>
						<sc:para xml:space="preserve">La température d'inversion est la température à partir de laquelle la réaction devient défavorable.</sc:para>
						<sc:para xml:space="preserve">Données à 298 K (on fait l'hypothèse que l'enthalpie ainsi que l'enthalpie libre de la réaction sont indépendants de la température) :</sc:para>
						<sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\varDelta_{r}G^{0}=-16,6\,\mathrm{kJ.mol^{-1}}</sc:textLeaf> ; <sc:textLeaf role="mathtex">\varDelta_{r}H^{0}=-42,6\,\mathrm{kJ.mol^{-1}}</sc:textLeaf>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">190 K</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Vous avez dû confondre enthalpie et enthalpie libre à la dernière étape du calcul.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">Il n'existe pas de température d'inversion.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">488 K</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">325 K</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Je ne vois pas comment vous avez pu aboutir à ce résultat.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="3"/>
		<sc:globalExplanation>
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment author="ves" creationTime="1528377221224">Explication pas très claire avec les liens. Il faudra peut-être ajouter le détail du calcul de la Ti
" La température d'invertion d'une réaction correspond à la températue où la réaction dévient défavorable, ce que signifie que \varDelta_{r}G^{0} =0. Nous avosn donc:
\varDelta_{r}G^{0}=0=\varDelta_{r}H^{0}-T\varDelta_{r}S^{0}
Vous avez les valuers d'entalpie et d'entalphie libre de formation à 298 K, vous pouvez donc calculer \varDelta_{r}S^{0} à 298.
Puis remplacer dans l'équation précedente pour avoir la température d'invertion"</comment></comment>-->
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Pour aller plus loin :</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_10.html</sp:url>
<sp:title>Loi de Hess</sp:title>
</op:urlM>Loi de Hess</sc:phrase>
</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch03/co/apprendre_ch3_11.html</sp:url>
<sp:title>Enthalpie standard de formation à 298 K</sp:title>
</op:urlM>Enthalpie standard de formation à 298 K</sc:phrase>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
