<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Technique expérimentale et mesure</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLicence>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimexpetech-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">1</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						<op:keywds>
							<sp:keywd>Technique expérimentale</sp:keywd>
							<sp:keywd>Mesure</sp:keywd>
							<sp:keywd>niveau facile</sp:keywd>
							<sp:keywd>méthodologie</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Le benzoate de méthyle (C<sc:textLeaf role="ind">6</sc:textLeaf>H<sc:textLeaf role="ind">5</sc:textLeaf>COOCH<sc:textLeaf role="ind">3</sc:textLeaf>) est obtenu par l'estérification de l'acide benzoïque (C<sc:textLeaf role="ind">6</sc:textLeaf>H<sc:textLeaf role="ind">5</sc:textLeaf>COOH) par du méthanol (CH<sc:textLeaf role="ind">3</sc:textLeaf>OH) en présence d'une quantité catalytique d'acide sulfurique à chaud.</sc:para>
					</op:txt>
				</sp:txt>
				<sp:txtRes xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txtRes>
						<op:txtResM>
							<sp:pos>lft</sp:pos>
						</op:txtResM>
						<sp:txt>
							<op:txt>
								<sc:para xml:space="preserve">Réaction d'estérification de l'acide benzoïque</sc:para>
							</op:txt>
						</sp:txt>
						<sp:img sc:refUri="UST-CMT-images/methodo_14.png"/>
					</op:txtRes>
				</sp:txtRes>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:table role="">
							<sc:column role="head" width="14"/>
							<sc:column width="14"/>
							<sc:column width="14"/>
							<sc:column width="14"/>
							<sc:column width="14"/>
							<sc:column width="14"/>
							<sc:column width="14"/>
							<sc:row role="head">
								<sc:cell role="word">
									<sc:para xml:space="preserve">Réactifs</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Masse Molaire</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Densité</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Masse</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Volume</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Quantité matière</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Équivalent</sc:para>
								</sc:cell>
							</sc:row>
							<sc:row>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g.mol<sc:textLeaf role="exp">-1</sc:textLeaf>
</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g.mL<sc:textLeaf role="exp">-1</sc:textLeaf>
</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">mL</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">mol</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
							</sc:row>
							<sc:row>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Acide benzoïque</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">122,12</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">24,42</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">0,20</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">1,0</sc:para>
								</sc:cell>
							</sc:row>
							<sc:row>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Méthanol</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">32,04</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">0,79</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">55,11</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">69,8</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">1,72</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">8,6</sc:para>
								</sc:cell>
							</sc:row>
							<sc:row>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Acide Sulfurique</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">3 gouttes</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
							</sc:row>
							<sc:row>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
							</sc:row>
							<sc:row role="head">
								<sc:cell role="word">
									<sc:para xml:space="preserve">Produits obtenus</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Masse Molaire</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Densité</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Masse</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Volume</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Quantité matière</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Rendement</sc:para>
								</sc:cell>
							</sc:row>
							<sc:row>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g.mol<sc:textLeaf role="exp">-1</sc:textLeaf>
</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g.mL<sc:textLeaf role="exp">-1</sc:textLeaf>
</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">g</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">mL</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">mol</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
							</sc:row>
							<sc:row>
								<sc:cell role="word">
									<sc:para xml:space="preserve">Benzoate de méthyle</sc:para>
								</sc:cell>
								<sc:cell role="word">
									<sc:para xml:space="preserve">136,00</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">21,79</sc:para>
								</sc:cell>
								<sc:cell role="word"/>
								<sc:cell role="word"/>
								<sc:cell role="word">
									<sc:para xml:space="preserve">80,0%</sc:para>
								</sc:cell>
							</sc:row>
						</sc:table>
						<sc:para xml:space="preserve">À partir des conditions réactionnelles décrites dans ce cahier de laboratoire déterminer la verrerie à utiliser pour additionner l'acide sulfurique :</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> Becher gradué de 10 mL</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> Éprouvette graduée de 1 mL</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> Pipette graduée de 1 mL</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> Pipette pasteur</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">Fiole jaugée de 100 mL</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Le volume d'acide sulfurique est de quelques gouttes (pas de volume précis) l'utilisation d'une pipette pasteur est suffisante.</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>