<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Acide / Base</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimexpetech-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>acide phosphorique</sp:keywd>
							<sp:keywd>courbe de spéciation</sp:keywd>
							<sp:keywd>chimie des solutions</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">L'acide phosphorique est un triacide faible symbolisé par <sc:textLeaf role="mathtex">H_{3}A</sc:textLeaf>. La figure ci-dessus montre le diagramme de spéciation de l'acide phosphorique de concentration initiale <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>. Quelles sont les espèces présentes en solution aux points A, B et C ainsi que leurs proportions?</sc:para>
					</op:txt>
				</sp:txt>
				<sp:res sc:refUri="UST-CHX-images/chim_exp_iii_35.png" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:resInfoM/>
				</sp:res>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">A : <sc:textLeaf role="mathtex">H_{3}A</sc:textLeaf> et <sc:textLeaf role="mathtex">H_{2}A^{-}</sc:textLeaf> en concentrations équimolaires <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. B : <sc:textLeaf role="mathtex">HA^{2-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>. C: <sc:textLeaf role="mathtex">A^{3-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">A : <sc:textLeaf role="mathtex">H_{3}A</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. B: <sc:textLeaf role="mathtex">HA^{2-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. C: <sc:textLeaf role="mathtex">A^{3-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">A : <sc:textLeaf role="mathtex">H_{3}A</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. B: <sc:textLeaf role="mathtex">H_{2}A^{-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf> et <sc:textLeaf role="mathtex">HA^{2-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. C: <sc:textLeaf role="mathtex">A^{3-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">A : <sc:textLeaf role="mathtex">H_{3}A</sc:textLeaf> et <sc:textLeaf role="mathtex">H_{2}A^{-}</sc:textLeaf> en concentrations équimolaires <sc:textLeaf role="mathtex">C_{0}/2</sc:textLeaf>. B : <sc:textLeaf role="mathtex">H_{2}A^{-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>. C: <sc:textLeaf role="mathtex">A^{3-}</sc:textLeaf> en concentration <sc:textLeaf role="mathtex">C_{0}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="4"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Le raisonnement est le même si on a affaire à un monoacide ou un polyacide.</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>