<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Couple redox</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimexpetech-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>synthèse du sulfate de cuivre</sp:keywd>
							<sp:keywd>oxydo-réduction</sp:keywd>
							<sp:keywd>chimie des solutions</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Les couples <sc:textLeaf role="mathtex">Cu^{2+}/Cu(s)</sc:textLeaf> et <sc:textLeaf role="mathtex">SO_{4}^{2-}/H_{2}SO_{3}</sc:textLeaf> ont des potentiels redox standards de <sc:textLeaf role="mathtex">+0.34</sc:textLeaf> et <sc:textLeaf role="mathtex">+0.17</sc:textLeaf> V, respectivement. En fonction de ces potentiels standards quelle est la réaction attendue entre <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> et <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> à température ambiante?</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">La réaction attendue est l'oxydation du <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> en <sc:textLeaf role="mathtex">Cu^{2+}</sc:textLeaf> et la réduction de <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> en <sc:textLeaf role="mathtex">H_{2}SO_{3}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">La réaction attendue est la réduction du <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> en <sc:textLeaf role="mathtex">Cu^{2+}</sc:textLeaf> et la réduction de <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> en <sc:textLeaf role="mathtex">H_{2}SO_{3}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">La réaction attendue est l'oxydation de <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> en <sc:textLeaf role="mathtex">H_{2}SO_{3}</sc:textLeaf> et la réduction du <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> en <sc:textLeaf role="mathtex">Cu^{2+}</sc:textLeaf>.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">Aucune réaction n'est possible dans ces conditions.</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="4"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">A température ambiante, aucune réaction n'est possible entre le <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> et <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> vu qu'en fonction des potentiels standards la réaction qui aura lieu est entre le <sc:textLeaf role="mathtex">Cu^{2+}</sc:textLeaf> (l'oxydant le plus fort) et <sc:textLeaf role="mathtex">H_{2}SO_{3}</sc:textLeaf> (le réducteur le plus fort). En revanche, en chauffant le mélange réactionnel, on s'éloigne des conditions standards et la réaction d'oxydation du <sc:textLeaf role="mathtex">Cu(s)</sc:textLeaf> par <sc:textLeaf role="mathtex">SO_{4}^{2-}</sc:textLeaf> devient possible.</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
