<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqSur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Activité des gaz parfaits</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-thermochim-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>Thermochimie</sp:keywd>
							<sp:keywd>Gaz parfait</sp:keywd>
							<sp:keywd>Pression partielle</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">15 g de <sc:textLeaf role="mathtex">NH_{3}</sc:textLeaf> gazeux (masse molaire <sc:textLeaf role="mathtex">17\,\mathrm{g.mol^{-1}}</sc:textLeaf>) sont enfermés dans un enceinte de 4L à la température de 600 K. Quelle est la pression du gaz ?</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">11 bar</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">11 mbar</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Attention à bien utiliser les unités SI, ici le volume doit être exprimé en m<sc:textLeaf role="exp">3</sc:textLeaf> et non en L. Une pression aussi faible devrait vous mettre la puce à l'oreille.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">187 bar</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">Attention à ne pas confondre masse et quantité de matière.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
			<sc:choice>
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">0,19 bar</sc:para>
					</op:txt>
				</sc:choiceLabel>
				<sc:choiceExplanation>
					<op:txt>
						<sc:para xml:space="preserve">2 confusions dans votre réponse, le volume doit être exprimé en <sc:textLeaf role="mathtex">m^{3}</sc:textLeaf> et il faut diviser la masse par la masse molaire pour obtenir la quantité de matière.</sc:para>
					</op:txt>
				</sc:choiceExplanation>
			</sc:choice>
		</sc:choices>
		<sc:solution choice="1"/>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Pour en savoir plus :</sc:para>
						<sc:para xml:space="preserve">
<sc:phrase role="url">
<op:urlM>
<sp:url>http://uel.unisciel.fr/chimie/chimther/chimther_ch02/co/apprendre_ch2_09.html</sp:url>
<sp:title>Calcul de l'activité d'un gaz</sp:title>
</op:urlM>Calcul de l'activité d'un gaz</sc:phrase>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqSur>
</sc:item>
