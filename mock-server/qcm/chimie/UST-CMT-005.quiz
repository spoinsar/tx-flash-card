<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Quantité de matière</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-strucmat-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">1</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						
						<op:keywds>
							<sp:keywd>méthodologie</sp:keywd>
							<sp:keywd>Quantité de matière</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">La masse atomique d&apos;un élément donné représente :</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">la masse d&apos;une mole d&apos;électrons de cet élément</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> la masse d&apos;une mole d&apos;atomes de cet élément</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> la masse de 6,02 × 10<sc:textLeaf role="exp">23</sc:textLeaf> atomes de cet élément</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve"> la masse de 6,02 × 10<sc:textLeaf role="exp">23</sc:textLeaf> électrons de cet élément</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Par définition, la masse molaire atomique <sc:textLeaf role="mathtex">M</sc:textLeaf> en (g.mol<sc:textLeaf role="exp">-1</sc:textLeaf>) d&apos;un élément représente la masse <sc:textLeaf role="mathtex">m</sc:textLeaf> en (g) d&apos;une mole d&apos;atomes <sc:textLeaf role="mathtex">n</sc:textLeaf> (mole) suivant <sc:textLeaf role="mathtex">M=\frac{m}{n}</sc:textLeaf> ou la masse de 6,02 × 10<sc:textLeaf role="exp">23</sc:textLeaf> atomes car <sc:textLeaf role="mathtex">M=\frac{m*N_A}{n}</sc:textLeaf> avec <sc:textLeaf role="mathtex">N_A</sc:textLeaf> la constante d&apos;Avogadro.</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>