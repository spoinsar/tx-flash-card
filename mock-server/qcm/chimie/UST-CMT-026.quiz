<?xml version="1.0" encoding="UTF-8"?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<op:mcqMur xmlns:op="utc.fr:ics/opale3">
		<op:exeM>
			<sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Solution aqueuse, proportion massique</sp:title>
			<sp:themeLycee xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-</sp:themeLycee>
			<sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#chim-chimsolu-</sp:themeLicence>
			<sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1537864737595">changement niveau de 1 à 3 car cacul à faire</comment></comment>-->3</sp:level>
			<sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L1</sp:educationLevel>
			<sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<op:info>
					<sp:keywds>
						<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1528725660290">enlever peut-être "niveau facile"</comment><comment author="ves" creationTime="1537864725809">"niveau facile" supprimé</comment></comment>-->
						<op:keywds>
							<sp:keywd>Solution aqueuse</sp:keywd>
							<sp:keywd>% massique</sp:keywd>
							<sp:keywd>méthodologie</sp:keywd>
						</op:keywds>
					</sp:keywds>
					<sp:cc>by</sp:cc>
					<sp:cpyrgt>
						<op:sPara>
							<sc:para xml:space="preserve">Université de Strasbourg</sc:para>
						</op:sPara>
					</sp:cpyrgt>
				</op:info>
			</sp:info>
		</op:exeM>
		<sc:question>
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="ves" creationTime="1522837206947">niveau 3</comment></comment>-->
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:para xml:space="preserve">Déterminer la concentration en mol/L d'une solution de peroxyde d'hydrogène à 20 % dans l'eau et de masse volumique ρ=1,11 g/cm<sc:textLeaf role="exp">3</sc:textLeaf>
</sc:para>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:question>
		<sc:choices>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">11,2 mol/L</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">8,8 mol/L</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="checked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">6,5 mol/L</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">15 mol/L</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
			<sc:choice solution="unchecked">
				<sc:choiceLabel>
					<op:txt>
						<sc:para xml:space="preserve">Aucune des réponses</sc:para>
					</op:txt>
				</sc:choiceLabel>
			</sc:choice>
		</sc:choices>
		<sc:globalExplanation>
			<op:res>
				<sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<op:txt>
						<sc:itemizedList>
							<sc:listItem>
								<sc:para xml:space="preserve">Par définition <sc:textLeaf role="mathtex">\text{x(\%}_{\text{massique soluté }(H_{2}O_{2})}\text{)}=\text{m}_{\text{soluté }(H_{2}O_{2})}/\text{(m}_{\text{totale solution}})</sc:textLeaf>, avec <sc:textLeaf role="mathtex">\text{m}_{\text{totale solution}}=(\rho_{\text{(solution)}}\times\text{V}_{\text{(solution)}})</sc:textLeaf> </sc:para>
								<sc:para xml:space="preserve">d'où <sc:textLeaf role="mathtex">\text{m}_{\text{soluté}}=\text{x(\%)}\times(\rho_\text{(solution)}\times \text{V}_{\text{(solution)}})</sc:textLeaf>.</sc:para>
							</sc:listItem>
						</sc:itemizedList>
						<sc:itemizedList>
							<sc:listItem>
								<sc:para xml:space="preserve">Remarque : Par définition la densité <sc:textLeaf role="mathtex">\text{d}=\rho\,\text{(solution)}/\rho\,\text{(eau)}</sc:textLeaf> avec <sc:textLeaf role="mathtex">\rho\,\text{(eau)}=1\,\text{g/mL à 4°C et 0,998 g/mL à 25°C}</sc:textLeaf> donc on peut assimiler <sc:textLeaf role="mathtex">\text{d (solution)}\approx\rho\,\text{(solution})/1</sc:textLeaf> à 25°C.</sc:para>
							</sc:listItem>
						</sc:itemizedList>
						<sc:itemizedList>
							<sc:listItem>
								<sc:para xml:space="preserve">AN : <sc:textLeaf role="mathtex">\text{m}_{\text{soluté}}\text{ dans un 1 litre}=0,2*1,1\text{ g/mL}*1000\text{ mL}=222\text{ g }H_{2}O_{2}\text{ dans 1 L}</sc:textLeaf>  de plus on a par définition : <sc:textLeaf role="mathtex">\text{[}H_{2}O_{2}\text{]}=\text{n}_{(H_{2}O_{2})}/\text{V}_{\text{(solution)}}</sc:textLeaf> et n<sc:textLeaf role="ind">H2O2</sc:textLeaf>=m<sc:textLeaf role="ind">H2O2</sc:textLeaf>/M<sc:textLeaf role="ind">H2O2</sc:textLeaf>
</sc:para>
								<sc:para xml:space="preserve"> d'où <sc:textLeaf role="mathtex">\text{[}H_{2}O_{2}\text{]}=(222/34,01)/1=6,5\text{ mol/L}</sc:textLeaf> avec <sc:textLeaf role="mathtex">\text{M}_{(H_{2}O_{2})}=34,01\text{ g/mol}</sc:textLeaf>.</sc:para>
							</sc:listItem>
						</sc:itemizedList>
					</op:txt>
				</sp:txt>
			</op:res>
		</sc:globalExplanation>
	</op:mcqMur>
</sc:item>
