const path_to_file_to_be_zipped = './qcm/chimie';

const makePromise = (fun, ...args) => new Promise((resolve, reject) => {
  fun(...args, (err, val) => {
    if (err) {
      reject(err);
    }
    resolve(val || true);
  })
})

const fs = require('fs');
const archiver = require('archiver');

const getArchive = (async () => {
  if(
    await makePromise(fs.access, path_to_file_to_be_zipped, fs.constants.F_OK | fs.constants.R_OK)
  ) {
    let archive;
    if ((await makePromise(fs.stat, path_to_file_to_be_zipped)).isDirectory()) {
      archive = archiver('zip', {
        zlib: { level: 9 },
      });
      archive.directory(path_to_file_to_be_zipped, false);
      archive.finalize();
    }
  }
});

module.exports = { getArchive };