const express = require('express')
const app = express()
const archiver = require('archiver')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/chimie', function(req, res) {
    
  const archive = archiver('zip');

  archive.on('error', function(err) {
    res.status(500).send({error: err.message});
  });

  //on stream closed we can end the request
  archive.on('end', function() {
  console.log('Archive wrote %d bytes', archive.pointer());
  });

  //set the archive name
  res.attachment('chimie.zip');

  //this is the streaming magic
  archive.pipe(res);

  const path_to_dir = __dirname + '/qcm/chimie';

  archive.directory(path_to_dir, false);

  archive.finalize();

});

app.listen(3002)

/*
const path = require('path');
const express = require('express')
const app = express()
const archiver = require('archiver')

app.get('/', function(req, res) {
    
  const archive = archiver('zip');

  archive.on('error', function(err) {
    res.status(500).send({error: err.message});
  });

  //on stream closed we can end the request
  archive.on('end', function() {
  console.log('Archive wrote %d bytes', archive.pointer());
  });

  //set the archive name
  res.attachment('archive-name.zip');

  //this is the streaming magic
  archive.pipe(res);

  const files = [__dirname + '/files/上午.png', __dirname + '/files/中午.json'];

  for(const i in files) {
    archive.file(files[i], { name: path.basename(files[i]) });
  }

  archive.finalize();

});

app.listen(3010)
*/